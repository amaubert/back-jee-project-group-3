FROM maven:3.6.3-jdk-13 as maven-builder
WORKDIR /workspace
COPY . /workspace/
RUN mvn package

FROM openjdk:11
WORKDIR /app
COPY --from=maven-builder /workspace/target /app
EXPOSE 8080
CMD ["sh", "-c","java   -DDB_TYPE=$DB_TYPE \
                        -DDB_HOST=$DB_HOST \
                        -DDB_PORT=$DB_PORT \
                        -DDB_NAME=$DB_NAME \
                        -DDB_USER=$DB_USER \
                        -DADDRESS_API=$ADDRESS_API \
                        -DJWT_SECRET_KEY=$JWT_SECRET_KEY \
                        -DSMS_API_BASE_URL=$SMS_API_BASE_URL \
                        -DSMS_API_KEY=$SMS_API_KEY -jar *.jar"]

