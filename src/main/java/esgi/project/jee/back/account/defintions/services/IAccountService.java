package esgi.project.jee.back.account.defintions.services;

import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;

import java.security.Principal;

public interface IAccountService {
    CodeToValidateDto subscribe(AccountSubscribeDto accountSubscribeDto);
    void resendActivationCode(Long accountId);
    void activate(Long accountId, AccountActivateDto accountActivateDto);
    AccountLoggedDto login(AccountLoginDto accountLoginDto);
    UserResponseDto me(Principal principal);
    void logout(String jwtToken);

    UserResponseDto updateMe(AccountUpdateDto accountUpdateDto, Principal principal);

}
