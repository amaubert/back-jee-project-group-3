package esgi.project.jee.back.account.exceptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AccountNotActivatedException extends UsernameNotFoundException {
    public AccountNotActivatedException(String message) {
        super(message);
    }

}
