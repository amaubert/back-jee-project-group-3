package esgi.project.jee.back.account.exceptions;

public class AccountNotMatchException extends RuntimeException {
    public AccountNotMatchException(String message) {
        super(message);
    }
}
