package esgi.project.jee.back.account.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class AccountAlreadyActivatedException extends ResourceAlreadyExistException {
    public AccountAlreadyActivatedException(String message) {
        super(message);
    }
}
