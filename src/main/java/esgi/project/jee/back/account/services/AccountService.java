package esgi.project.jee.back.account.services;

import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.account.exceptions.AccountAlreadyExistException;
import esgi.project.jee.back.account.exceptions.AccountNotActivatedException;
import esgi.project.jee.back.user.definitions.dtos.UserCreateDto;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.account.defintions.services.IAccountService;
import esgi.project.jee.back.token.definitions.services.ITokenService;
import esgi.project.jee.back.user.definitions.services.IUserService;
import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.exceptions.AccountAlreadyActivatedException;
import esgi.project.jee.back.account.exceptions.AccountNotFoundException;
import esgi.project.jee.back.account.exceptions.AccountNotMatchException;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.confirmationCode.definitions.services.IConfirmationCodeService;
import esgi.project.jee.back.security.definitions.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService implements UserDetailsService, IAccountService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    private final IConfirmationCodeService confirmationCodeService;
    private final IUserService userService;
    private final AuthenticationManagerBuilder authenticationManager;
    private final ITokenService tokenService;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          PasswordEncoder passwordEncoder,
                          IConfirmationCodeService confirmationCodeService,
                          IUserService userService,
                          AuthenticationManagerBuilder authenticationManager,
                          ITokenService tokenService) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
        this.confirmationCodeService = confirmationCodeService;
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
    }

    @Transactional
    @Override
    public CodeToValidateDto subscribe(AccountSubscribeDto accountSubscribeDto) {
        //Watch if the account already Exist
        accountRepository.findByLogin(accountSubscribeDto.getLogin())
                                .ifPresent(account -> {
                                    throw new AccountAlreadyExistException("account already exist .");
                                });

        //Create the account
        var account = new Account(accountSubscribeDto);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account = accountRepository.save(account);

        var accountDto = new AccountDto(account);

        //Create the user
        var userCreateDto = new UserCreateDto(accountSubscribeDto, accountDto);
        var userDto = userService.create(userCreateDto);

        //Send SMS code to Validate the account
        var confirmationCodeDto = confirmationCodeService.create(account);
        confirmationCodeService.sendCode(confirmationCodeDto.getCode(), userDto);

        return new CodeToValidateDto(userDto);
    }

    @Override
    public void resendActivationCode(Long accountId) {
        var userDto = userService.findByAccountId(accountId);
        confirmationCodeService.resendCode(userDto);
    }

    @Transactional
    @Override
    public void activate(Long accountId, AccountActivateDto accountActivateDto) {

        var accountFetched = accountRepository.findById(accountId)
                                                .orElseThrow(() -> new AccountNotFoundException("Account with id (" + accountId + ") not found ."));
        if (accountFetched.isActivated()) {
            throw new AccountAlreadyActivatedException("Account already activated .");
        }
        var account = confirmationCodeService.confirm(accountActivateDto.getCode());

        if (!account.equals(accountFetched)) {
            throw new AccountNotMatchException("Account not match .");
        }

        account.setActivated(true);
        accountRepository.save(account);
    }

    @Override
    public AccountLoggedDto login(AccountLoginDto accountLoginDto) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(  accountLoginDto.getLogin(),
                                                                            accountLoginDto.getPassword());
        Authentication authentication = authenticationManager.getObject().authenticate(authenticationToken);

        var account = accountRepository.findByLogin(accountLoginDto.getLogin())
                                        .orElseThrow(() -> new AccountNotFoundException("Account with login (" +
                                                                     accountLoginDto.getLogin() + ") not found ."));

        if (!account.isActivated()) {
            throw new AccountNotActivatedException("account is not activated yet .");
        }
        var token = tokenService.create(authentication, account);

        return new AccountLoggedDto(token.getToken());
    }

    @Override
    public UserResponseDto me(Principal principal) {
        var account = accountRepository.findByLogin(principal.getName())
                                    .orElseThrow(() -> new AccountNotFoundException("Account with login (" +
                                                                                    principal.getName() + ") not found ."));

        var userDto = userService.findByAccountId(account.getId());
        return new UserResponseDto(userDto);
    }

    @Override
    public void logout(String jwtToken) {
        tokenService.delete(jwtToken);
    }

    @Transactional
    @Override
    public UserResponseDto updateMe(AccountUpdateDto accountUpdateDto,
                                    Principal principal) {

        var userDto = userService.update(accountUpdateDto, principal.getName());

        var account = accountRepository.findByLogin(principal.getName())
                                        .orElseThrow(() -> new AccountNotFoundException("Account with login (" +
                                                principal.getName() + ") not found ."));

        updateAccount(account, accountUpdateDto);

        account = accountRepository.save(account);

        userDto.setAccount(new AccountDto(account));
        return new UserResponseDto(userDto);
    }

    private void updateAccount(Account account,AccountUpdateDto accountUpdateDto) {
        if( accountUpdateDto.getLogin() != null && !account.getLogin().equals(accountUpdateDto.getLogin())) {
           account.setLogin(accountUpdateDto.getLogin());
        }
        if( accountUpdateDto.getPassword() != null &&
            !passwordEncoder.matches(accountUpdateDto.getPassword(), account.getPassword()) ){
            account.setPassword(passwordEncoder.encode(accountUpdateDto.getPassword()));
        }
        if(account.isAdmin()  && account.isAdmin() != accountUpdateDto.getIsAdmin()){
            account.setAdmin(accountUpdateDto.getIsAdmin());
        }
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
       var account =  accountRepository.findByLogin(login)
                            .orElseThrow(() -> new UsernameNotFoundException("Login Incorrect ."));
       var roles = rolesFromAccount(account);

       return User.builder()
                   .username(account.getLogin())
                   .password(account.getPassword())
                   .roles(roles.toArray(new String[0]))
                   .accountExpired(false)
                   .accountLocked(false)
                   .credentialsExpired(false)
                   .disabled(false)
                   .build();
    }

    private List<String> rolesFromAccount(Account account) {
        var roles = new java.util.ArrayList<>(List.of(Roles.User));
        if ( account.isAdmin()) {
            roles.add(Roles.Admin);
        }

        return roles.stream()
                        .map(Enum::name)
                        .collect(Collectors.toList());
    }
}
