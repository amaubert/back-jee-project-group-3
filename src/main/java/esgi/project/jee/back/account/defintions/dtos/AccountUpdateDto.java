package esgi.project.jee.back.account.defintions.dtos;

import esgi.project.jee.back.share.validations.Email;
import esgi.project.jee.back.share.validations.Password;
import esgi.project.jee.back.share.validations.Phone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountUpdateDto {

    @Size(min = 3, max = 30, message = "{login.invalid}")
    private String login;

    @Password(message = "{password.invalid}")
    private String password;

    private String lastName;

    private String firstName;

    @Phone(message = "{phone.invalid}")
    private String phone;

    @Email(message = "{email.invalid}")
    private String email;

    private boolean isAdmin;

    private String sponsorshipCode;

    public boolean getIsAdmin() {
        return isAdmin;
    }
}
