package esgi.project.jee.back.account.defintions.dtos;

import esgi.project.jee.back.share.validations.Phone;
import esgi.project.jee.back.share.validations.Email;
import esgi.project.jee.back.share.validations.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountSubscribeDto {

    @NotNull(message = "{login.null}")
    @NotEmpty(message = "{login.empty}")
    @Size(min = 3, max = 30, message = "{login.invalid}")
    private String login;

    @NotNull(message = "{password.null}")
    @NotEmpty(message = "{password.empty}")
    @Password(message = "{password.invalid}")
    private String password;

    @NotNull(message = "{last-name.null}")
    @NotEmpty(message = "{last-name.empty}")
    private String lastName;

    @NotNull(message = "{first-name.null}")
    @NotEmpty(message = "{first-name.empty}")
    private String firstName;

    @NotNull(message = "{phone.null}")
    @NotEmpty(message = "{phone.empty}")
    @Phone(message = "{phone.invalid}")
    private String phone;

    @NotNull(message = "{email.null}")
    @NotEmpty(message = "{email.empty}")
    @Email(message = "{email.invalid}")
    private String email;

    private boolean isAdmin;

    private String sponsorshipCode;

    //Add this else conflict between Lombok and Jackson JSON conversion
    //isAdmin is know as admin
    public boolean getIsAdmin() {
        return isAdmin;
    }

    public AccountSubscribeDto(@NotNull @NotEmpty @Size(min = 3, max = 30) String login, @NotNull @NotEmpty @Size(min = 8, max = 30) String password, @NotNull @NotEmpty String lastName, @NotNull @NotEmpty String firstName, @NotNull @NotEmpty @Pattern(regexp = "^33[1-9]([-. ]?[0-9]{2}){4}$") String phone, @NotNull @NotEmpty @Pattern(regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$") String email, boolean isAdmin) {
        this.login = login;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phone = phone;
        this.email = email;
        this.isAdmin = isAdmin;
    }
}

