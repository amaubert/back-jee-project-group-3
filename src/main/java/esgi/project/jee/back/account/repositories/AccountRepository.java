package esgi.project.jee.back.account.repositories;

import esgi.project.jee.back.account.entities.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    Optional<Account> findByLogin(String login);
}
