package esgi.project.jee.back.account.entities;

import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.account.defintions.dtos.AccountSubscribeDto;
import esgi.project.jee.back.account.defintions.dtos.AccountUpdateDto;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity(name = "account")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Account extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column( columnDefinition = "boolean default false")
    private boolean isAdmin;

    @Column( columnDefinition = "boolean default false")
    private boolean isActivated;

    public Account(AccountSubscribeDto accountSubscribeDto) {
        this.isAdmin = accountSubscribeDto.getIsAdmin();
        this.login = accountSubscribeDto.getLogin();
        this.password = accountSubscribeDto.getPassword();
    }

    public Account(AccountDto account) {
        this.id = account.getId();
        this.isAdmin = account.isAdmin();
        this.login = account.getLogin();
        this.password = account.getPassword();
        this.isActivated = account.isActivated();
    }

}
