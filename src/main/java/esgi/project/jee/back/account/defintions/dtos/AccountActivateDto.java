package esgi.project.jee.back.account.defintions.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountActivateDto {
    @NotNull(message = "{activation-code.null}")
    @NotEmpty(message = "{activation-code.empty}")
    @Size(min = 6, max = 6, message = "{activation-code.invalid}")
    private String code;

}
