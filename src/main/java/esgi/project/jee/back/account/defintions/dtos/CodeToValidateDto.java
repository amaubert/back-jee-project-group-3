package esgi.project.jee.back.account.defintions.dtos;

import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodeToValidateDto {
    private String validationUrl;
    private UserResponseDto user;

    public CodeToValidateDto(UserDto userDto) {
        this.validationUrl = "";
        this.user = new UserResponseDto(userDto);
    }
}
