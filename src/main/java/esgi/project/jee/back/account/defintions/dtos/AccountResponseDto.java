package esgi.project.jee.back.account.defintions.dtos;

import esgi.project.jee.back.account.entities.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponseDto {
    private Long id;
    private boolean isAdmin;
    private boolean isActivated;

    public AccountResponseDto(AccountDto account) {
        this.id = account.getId();
        this.isAdmin = account.isAdmin();
        this.isActivated = account.isActivated();
    }

    public AccountResponseDto(Account account) {
        this.id = account.getId();
        this.isAdmin = account.isAdmin();
        this.isActivated = account.isActivated();
    }
}
