package esgi.project.jee.back.account.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class AccountAlreadyExistException extends ResourceAlreadyExistException {
    public AccountAlreadyExistException(String message) {
        super(message);
    }
}
