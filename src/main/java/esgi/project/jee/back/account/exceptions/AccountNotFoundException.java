package esgi.project.jee.back.account.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class AccountNotFoundException extends ResourceNotFoundException {
    public AccountNotFoundException(String message) {
        super(message);
    }
}
