package esgi.project.jee.back.account.controllers;

import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.account.defintions.services.IAccountService;
import esgi.project.jee.back.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.security.Principal;

import static esgi.project.jee.back.share.Routes.Account.*;
import static esgi.project.jee.back.security.TokenProvider.Token_Prefix;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = Prefix, produces = APPLICATION_JSON_VALUE)
public class AccountController {

    private final IAccountService accountService;

    @Autowired
    public AccountController(IAccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(Subscribe)
    public ResponseEntity<CodeToValidateDto> subscribe(@RequestBody @Valid AccountSubscribeDto accountSubscribeDto,
                                                       UriComponentsBuilder uriBuilder){
        var codeToValidateDto = accountService.subscribe(accountSubscribeDto);
        var account = codeToValidateDto.getUser().getAccount();
        var uri  = uriBuilder.path(completeUri(Activate))
                                .buildAndExpand(account.getId())
                                .toUri();
        codeToValidateDto.setValidationUrl(uri.toString());

        return ResponseEntity.created(uri).body(codeToValidateDto);
    }

    @PostMapping("/{accountId}/resend-code")
    public ResponseEntity<?> resendActivationCode(@PathVariable("accountId") Long accountId) {
        accountService.resendActivationCode(accountId);
        return ResponseEntity.ok().build();
    }

    @PutMapping(Activate)
    public ResponseEntity<?> activate(@RequestBody @Valid AccountActivateDto accountActivateDto,
                                      @PathVariable("accountId") Long accountId) {
        accountService.activate(accountId, accountActivateDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(Login)
    public ResponseEntity<AccountLoggedDto> login(@RequestBody @Valid AccountLoginDto accountLoginDto) {
        var accountLoggedDto = accountService.login(accountLoginDto);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION, Token_Prefix + accountLoggedDto.getToken());
        return new ResponseEntity<>(accountLoggedDto, httpHeaders, OK);
    }

    @GetMapping(Me)
    public ResponseEntity<UserResponseDto> me(Principal principal) {
        var userResponseDto = accountService.me(principal);
        return ResponseEntity.ok(userResponseDto);
    }

    @PutMapping(Me)
    public ResponseEntity<UserResponseDto> updateMe(@RequestBody @Valid AccountUpdateDto AccountUpdateDto,
                                                    Principal principal) {
        var userResponseDto = accountService.updateMe(AccountUpdateDto, principal);
        return ResponseEntity.ok(userResponseDto);
    }

    @PostMapping(Logout)
    public ResponseEntity<?> logout(HttpServletRequest httpServletRequest) {
        String jwtToken = TokenProvider.resolveToken(httpServletRequest);
        accountService.logout(jwtToken);
        return ResponseEntity.noContent().build();
    }
}
