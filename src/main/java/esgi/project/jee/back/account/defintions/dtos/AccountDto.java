package esgi.project.jee.back.account.defintions.dtos;

import esgi.project.jee.back.account.entities.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private Long id;
    private boolean isAdmin;
    private boolean isActivated;
    private String login;
    private String password;


    public AccountDto(Account account) {
        this.id = account.getId();
        this.isAdmin = account.isAdmin();
        this.login = account.getLogin();
        this.password = account.getPassword();
        this.isActivated = account.isActivated();
    }
}
