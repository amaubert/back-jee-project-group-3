package esgi.project.jee.back.sponsorship.definitions.dtos;


import esgi.project.jee.back.share.validations.Phone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SponsorshipCreateDto {

    @NotNull(message = "{send-to.null}")
    @NotEmpty(message = "{send-to.empty}")
    @Phone(message = "{send-to.invalid}")
    private String sendTo;
}
