package esgi.project.jee.back.sponsorship.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class SponsorshipNotFoundException extends ResourceNotFoundException {
    public SponsorshipNotFoundException(String message) {
        super(message);
    }
}
