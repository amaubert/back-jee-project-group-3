package esgi.project.jee.back.sponsorship.definitions.services;

import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.entities.User;


public interface ISponsorshipService {
    SponsorShipDto createSponsorshipCode(User user, String sendTo) throws ResourceNotFoundException;
    void linkSponsorship(String code, User user) throws ResourceNotFoundException;
    boolean isSponsored(UserDto userDto);
}
