package esgi.project.jee.back.sponsorship.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class SponsorshipAlreadyExistException extends ResourceAlreadyExistException  {
    public SponsorshipAlreadyExistException(String message) {
        super(message);
    }
}
