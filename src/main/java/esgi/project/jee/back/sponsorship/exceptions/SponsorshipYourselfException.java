package esgi.project.jee.back.sponsorship.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class SponsorshipYourselfException extends ResourceAlreadyExistException {
    public SponsorshipYourselfException(String message) {
        super(message);
    }
}
