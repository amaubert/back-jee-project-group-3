package esgi.project.jee.back.sponsorship.services;

import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import esgi.project.jee.back.share.plateforms.sms.exceptions.SendSmsFailedException;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.services.ISponsorshipService;
import esgi.project.jee.back.sponsorship.exceptions.SponsorshipAlreadyExistException;
import esgi.project.jee.back.sponsorship.exceptions.SponsorshipNotFoundException;
import esgi.project.jee.back.sponsorship.exceptions.SponsorshipYourselfException;
import esgi.project.jee.back.sponsorship.repositories.SponsorshipRepository;
import esgi.project.jee.back.sponsorship.entities.Sponsorship;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.utils.OtpCodeUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SponsorshipService implements ISponsorshipService {

    private final SponsorshipRepository sponsorshipRepository;
    private final ISMSSender smsService;

    public SponsorshipService( SponsorshipRepository sponsorshipRepository,
                               ISMSSender smsService) {
        this.sponsorshipRepository = sponsorshipRepository;
        this.smsService = smsService;
    }

    @Transactional
    @Override
    public SponsorShipDto createSponsorshipCode(User user, String sendTo) {
        //TODO change Exception
        if( user.getPhone().equals(sendTo) ) {
            throw new SponsorshipYourselfException("You can't sponsor yourself !");
        }

        //Generate sponsorship code
        var code = OtpCodeUtil.generateCode();
        var codeFound = sponsorshipRepository.findByCode(code);
        while (codeFound.isPresent()) {
            code = OtpCodeUtil.generateCode();
            codeFound = sponsorshipRepository.findByCode(code);
        }
        //Create a sponsorship in base
        var sponsorship = new Sponsorship(code, user);
        sponsorship = sponsorshipRepository.save(sponsorship);

        //sens sponsorship code by sms
        var userDto = new UserDto(sponsorship.getSponsor());
        var content = messageContent(code, userDto);
        var smsResponse = smsService.send(sendTo, content);

        var body = smsResponse.getBody();

        if( body == null || body.getMessages() == null || body.getMessages().size() != 1) {
            sponsorshipRepository.delete(sponsorship);
            throw new SendSmsFailedException("sms send to : " + sendTo + " failed .");
        }

        return new SponsorShipDto(sponsorship.getCode(), sponsorship.getSponsor());
    }

    @Override
    public void linkSponsorship(String code, User user) {
        var sponsorship = sponsorshipRepository.findByCode(code)
                                    .orElseThrow(() -> new SponsorshipNotFoundException("sponsorship code : '" +
                                                                                        code +
                                                                                     "' not found."));
        if (sponsorship.getSponsored() != null) {
            throw new SponsorshipAlreadyExistException(sponsorship.getSponsor().getFirstName() +
                                        " " + sponsorship.getSponsor().getLastName() +
                                        " has already a sponsorship with another user");
        }
        sponsorship.setSponsored(user);
        sponsorshipRepository.save(sponsorship);
    }

    @Override
    public boolean isSponsored(UserDto userDto) {
        var optionalSponsorship = sponsorshipRepository.findBySponsored_Id(userDto.getId());
        return optionalSponsorship.isPresent();
    }

    private String messageContent(String code, UserDto user) {
        var stringBuilder = new StringBuilder();
        stringBuilder.append("Where Is My Home");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("Vous avez reçu un code de parrainage de la part de ");
        stringBuilder.append(user.getFirstName());
        stringBuilder.append(" ");
        stringBuilder.append( user.getLastName());
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("code : ");
        stringBuilder.append(code);
        return stringBuilder.toString();
    }
}
