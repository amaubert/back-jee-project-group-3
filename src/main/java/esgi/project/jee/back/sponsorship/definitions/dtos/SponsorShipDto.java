package esgi.project.jee.back.sponsorship.definitions.dtos;

import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.user.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SponsorShipDto {
    private String code;
    private UserResponseDto sponsor;

    public SponsorShipDto(String code, User sponsor) {
        this.code = code;
        this.sponsor = new UserResponseDto(new UserDto(sponsor));
    }
}
