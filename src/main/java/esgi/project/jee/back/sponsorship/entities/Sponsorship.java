package esgi.project.jee.back.sponsorship.entities;

import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Sponsorship extends BaseEntity {

    @Column( unique = true, nullable = false, length = 6)
    private String code;

    @OneToOne
    @JoinColumn(nullable = false,
                foreignKey = @ForeignKey(name = "fk_sponsorship_sponsor_user",
                    value = ConstraintMode.CONSTRAINT))
    private User sponsor;

    @OneToOne
    @JoinColumn( foreignKey = @ForeignKey(name = "fk_sponsorship_sponsored_user",
                    value = ConstraintMode.CONSTRAINT))
    private User sponsored;


    public Sponsorship(String code, User sponsor) {
        this.code = code;
        this.sponsor = sponsor;
    }
}
