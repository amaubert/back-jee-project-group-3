package esgi.project.jee.back.sponsorship.repositories;

import esgi.project.jee.back.sponsorship.entities.Sponsorship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SponsorshipRepository extends CrudRepository<Sponsorship, Long> {
    Optional<Sponsorship> findByCode(String code);
    Optional<Sponsorship> findBySponsored_Id(Long id);
}
