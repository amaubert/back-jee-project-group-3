package esgi.project.jee.back.confirmationCode.exceptions;

public class SendCodeFailedException extends RuntimeException {
    public SendCodeFailedException(String message) {
        super(message);
    }
}
