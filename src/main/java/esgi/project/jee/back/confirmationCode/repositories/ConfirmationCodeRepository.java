package esgi.project.jee.back.confirmationCode.repositories;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.confirmationCode.entities.ConfirmationCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationCodeRepository extends CrudRepository<ConfirmationCode, Long> {
    Optional<ConfirmationCode> findByCode(String code);
    Optional<ConfirmationCode> findByAccount_Id(Long accountId);
}
