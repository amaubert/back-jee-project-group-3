package esgi.project.jee.back.confirmationCode.definitions.services;


import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.confirmationCode.definitions.dtos.ConfirmationCodeDto;

public interface IConfirmationCodeService {
    void sendCode(String code, UserDto userDto);
    void resendCode(UserDto userDto);
    ConfirmationCodeDto create(Account account);
    Account confirm(String code);
}
