package esgi.project.jee.back.confirmationCode.entities;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ConfirmationCode extends BaseEntity {

    @Size(min = 6, max = 6)
    @Column(unique = true, nullable = false)
    private String code;

    @OneToOne( fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false,
            foreignKey = @ForeignKey(name = "fk_confirmationCode_account",
                    value = ConstraintMode.CONSTRAINT))
    private Account account;

    public ConfirmationCode(String code,
                            Account account) {
        this.code = code;
        this.account = account;
    }
}
