package esgi.project.jee.back.confirmationCode.services;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.confirmationCode.definitions.dtos.ConfirmationCodeDto;
import esgi.project.jee.back.confirmationCode.definitions.services.IConfirmationCodeService;
import esgi.project.jee.back.confirmationCode.entities.ConfirmationCode;
import esgi.project.jee.back.confirmationCode.exceptions.ConfirmationCodeNotFoundException;
import esgi.project.jee.back.confirmationCode.repositories.ConfirmationCodeRepository;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import esgi.project.jee.back.share.plateforms.sms.exceptions.SendSmsFailedException;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class ConfirmationCodeService implements IConfirmationCodeService {
    private final ConfirmationCodeRepository confirmationCodeRepository;
    private final ISMSSender smsSender;
    private final static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final Random random;

    @Autowired
    public ConfirmationCodeService(ConfirmationCodeRepository confirmationCodeRepository,
                                   ISMSSender smsSender){
        this.confirmationCodeRepository = confirmationCodeRepository;
        this.smsSender = smsSender;
        random = new Random();
    }

    @Override
    public void sendCode(String code, UserDto userDto) {
        String content = "code : " + code;
        var response = smsSender.send(userDto.getPhone(), content);

        var body = response.getBody();
        if( body == null || body.getMessages() == null || body.getMessages().size() != 1) {
            throw new SendSmsFailedException("Send Code failed .");
        }
        var smsResponseDto = body.getMessages().get(0);
        if(!smsResponseDto.accepted) {
            throw new SendSmsFailedException("Send Code failed .");
        }
    }

    @Override
    public void resendCode(UserDto userDto) {
        var confirmationCode = confirmationCodeRepository.findByAccount_Id(userDto.getAccount().getId())
                                    .orElseThrow(() -> new ConfirmationCodeNotFoundException("Confirmation code not found ."));
        sendCode(confirmationCode.getCode(), userDto);
    }

    @Override
    public ConfirmationCodeDto create(Account account) {
        var code = generateCode();
        var codeExist = confirmationCodeRepository.findByCode(code);
        while (codeExist.isPresent()) {
            code = generateCode();
            codeExist = confirmationCodeRepository.findByCode(code);
        }
        var confirmationCode = new ConfirmationCode(code, account);
        confirmationCode = confirmationCodeRepository.save(confirmationCode);
        return new ConfirmationCodeDto(confirmationCode);
    }

    private String generateCode() {
        StringBuilder stringBuilder = new StringBuilder();
        var randomNumber = Math.abs(random.nextInt()) % alphabet.length();
        stringBuilder.append(alphabet.charAt(randomNumber));
        for( int i = 0 ; i < 5; i++) {
            randomNumber = Math.abs(random.nextInt()) % alphabet.length();
            stringBuilder.append(alphabet.charAt(randomNumber));
        }
        return stringBuilder.toString();
    }

    @Override
    public Account confirm(String code)  {
        var  confirmationCode = confirmationCodeRepository.findByCode(code)
                .orElseThrow(() -> new ConfirmationCodeNotFoundException("Confirmation with code ("+
                        code + ") not found ."));
        confirmationCodeRepository.delete(confirmationCode);
        return confirmationCode.getAccount();
    }

}
