package esgi.project.jee.back.confirmationCode.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class ConfirmationCodeNotFoundException extends ResourceNotFoundException {
    public ConfirmationCodeNotFoundException(String message) {
        super(message);
    }
}
