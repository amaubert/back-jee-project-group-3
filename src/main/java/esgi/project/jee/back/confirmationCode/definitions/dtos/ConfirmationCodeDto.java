package esgi.project.jee.back.confirmationCode.definitions.dtos;

import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.confirmationCode.entities.ConfirmationCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmationCodeDto {
    private Long id;
    private String code;
    private AccountDto account;

    public ConfirmationCodeDto(ConfirmationCode confirmationCode) {
        this.id = confirmationCode.getId();
        this.code = confirmationCode.getCode();
        this.account = new AccountDto(confirmationCode.getAccount());
    }
}
