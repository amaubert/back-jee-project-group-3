package esgi.project.jee.back.amenity.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class AmenityAlreadyExistException  extends ResourceAlreadyExistException {
    public AmenityAlreadyExistException(String message) {
        super(message);
    }
}
