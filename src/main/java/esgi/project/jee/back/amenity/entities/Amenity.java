package esgi.project.jee.back.amenity.entities;

import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Amenity")
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Amenity extends BaseEntity {
    @NotNull
    private String type;

    @NotNull
    private String name;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(
            foreignKey = @ForeignKey(name = "fk_property_address",
                    value = ConstraintMode.CONSTRAINT))
    private Address address;

    public Amenity() {}

    public Amenity(AmenityCreateDto amenityCreateDto) {
        this.type = amenityCreateDto.getType();
        this.name = amenityCreateDto.getName();
    }

    public Amenity(AmenityDto amenityDto) {
        this.id = amenityDto.getId();
        this.type = amenityDto.getType();
        this.name = amenityDto.getName();
        this.address = new Address(amenityDto.getAddress());
    }
}
