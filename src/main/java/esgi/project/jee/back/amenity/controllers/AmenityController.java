package esgi.project.jee.back.amenity.controllers;

import esgi.project.jee.back.share.Routes;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityUpdateDto;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.amenity.definitions.IAmenityService;
import esgi.project.jee.back.amenity.exceptions.AmenityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping( path = Routes.Amenity.Prefix ,produces = MediaType.APPLICATION_JSON_VALUE)
public class AmenityController {

    private final IAmenityService amenityService;

    @Autowired
    public AmenityController(IAmenityService amenityService, IAddressService addressService) {
        this.amenityService = amenityService;
    }

    @PostMapping()
    public ResponseEntity<AmenityDto> create(@RequestBody @Valid AmenityCreateDto amenityCreateDto,
                                       UriComponentsBuilder uriBuilder) {
        var amenityDto = amenityService.create(amenityCreateDto);

        //TODO Amenity Fetch By ID ???
        var uri = uriBuilder.path(Routes.Amenity.Prefix).build().toUri();
        return ResponseEntity.created(uri).body(amenityDto);
    }

    @GetMapping
    public ResponseEntity<?> list(){
        var amenityList = amenityService.findAllAmenities();
        return ResponseEntity.ok().body(amenityList);
    }

    @DeleteMapping(Routes.Amenity.Delete)
    public ResponseEntity<Long> deleteAmenityById(@PathVariable Long amenityId) throws AmenityNotFoundException {
        amenityService.deleteAmenityById(amenityId);
        return ResponseEntity.status(OK).build();
    }

    @PutMapping(Routes.Amenity.Update)
    public ResponseEntity<AmenityDto> update( @PathVariable("amenityId") Long id,
                                               @RequestBody @Valid AmenityUpdateDto amenityUpdateDto) throws AmenityNotFoundException {
        var propertyDto = amenityService.update(id, amenityUpdateDto);
        return ResponseEntity.ok(propertyDto);
    }
}
