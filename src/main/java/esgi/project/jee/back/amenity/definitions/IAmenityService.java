package esgi.project.jee.back.amenity.definitions;

import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityUpdateDto;
import esgi.project.jee.back.amenity.exceptions.AmenityNotFoundException;

import java.util.List;

public interface IAmenityService {
    AmenityDto findAmenityById(Long id) throws AmenityNotFoundException;
    List<AmenityDto> findAllAmenities();
    AmenityDto create(AmenityCreateDto amenityCreateDto);
    void deleteAmenityById(Long amenityId) throws AmenityNotFoundException;
    AmenityDto update(Long id, AmenityUpdateDto amenityUpdateDto) throws AmenityNotFoundException;
    List<AmenityDto> findAllAmenitiesByRange(double quotientLatitude, double quotientLongitude, double distance);
}
