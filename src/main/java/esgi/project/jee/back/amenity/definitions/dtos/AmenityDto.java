package esgi.project.jee.back.amenity.definitions.dtos;

import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.amenity.entities.Amenity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AmenityDto {

    @NotNull
    private Long id;

    @NotNull
    private String type;

    @NotNull
    private String name;

    @NotNull
    @Valid
    private AddressDto address;

    public AmenityDto( @NotNull Amenity amenity) {
        this.id = amenity.getId();
        this.type = amenity.getType();
        this.name = amenity.getName();
        this.address = new AddressDto(amenity.getAddress());
    }
}
