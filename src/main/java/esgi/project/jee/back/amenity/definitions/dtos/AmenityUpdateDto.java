package esgi.project.jee.back.amenity.definitions.dtos;

import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenityUpdateDto {
    @NotNull(message = "{type.null}")
    @NotEmpty(message = "{type.empty}")
    private String type;

    @NotNull(message = "{name.null}")
    @NotEmpty(message = "{name.empty}")
    private String name;

    @Valid
    private AddressCreateDto address;

    public AmenityUpdateDto(AmenityDto amenityDto) {
        this.type = amenityDto.getType();
        this.name = amenityDto.getName();
        this.address = new AddressCreateDto(amenityDto.getAddress());
    }
}
