package esgi.project.jee.back.amenity.repositories;

import esgi.project.jee.back.amenity.entities.Amenity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AmenityRepository extends CrudRepository<Amenity, Long> {
    Optional<Amenity> findByTypeAndNameAndAddressId(String type, String name, Long addressId);
    List<Amenity> findAll();

    @Query("SELECT a FROM Amenity a WHERE a.address.latitude BETWEEN (?1 - (?3 /?4)) AND (?1 + (?3 /?4)) AND "
                                      + " a.address.longitude BETWEEN (?2 - (?3 / ?5)) AND (?2 + (?3 / ?5))")
    List<Amenity> findByRange(double latitude, double longitude, double distance,
                              double factorLatitude, double factorLongitude);
}
