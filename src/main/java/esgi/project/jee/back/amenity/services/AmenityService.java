package esgi.project.jee.back.amenity.services;

import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityUpdateDto;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.amenity.definitions.IAmenityService;
import esgi.project.jee.back.address.exceptions.AddressCreationFailedException;
import esgi.project.jee.back.amenity.exceptions.AmenityAlreadyExistException;
import esgi.project.jee.back.amenity.exceptions.AmenityNotFoundException;
import esgi.project.jee.back.amenity.repositories.AmenityRepository;
import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.amenity.entities.Amenity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AmenityService implements IAmenityService {

    private final AmenityRepository amenityRepository;
    private final IAddressService addressService;

    @Autowired
    public AmenityService(AmenityRepository amenityRepository,
                          IAddressService addressService) {
        this.amenityRepository = amenityRepository;
        this.addressService = addressService;
    }

    public Optional<Amenity> fetchAmenity(String type, String name, Long addressId) {
        return this.amenityRepository.findByTypeAndNameAndAddressId(type, name, addressId);
    }

    @Override
    public AmenityDto findAmenityById(Long amenityId) throws AmenityNotFoundException {
        Amenity amenity =  this.amenityRepository.findById(amenityId)
                .orElseThrow(() -> new AmenityNotFoundException("Amenity with ID (" + amenityId + ") not found ."));
        return new AmenityDto(amenity);
    }

    @Override
    public List<AmenityDto> findAllAmenities() {
        return this.amenityRepository.findAll().stream()
                                                .map(AmenityDto::new)
                                                .collect(Collectors.toList());
    }

    @Override
    public AmenityDto create(AmenityCreateDto amenityCreateDto) {
        AddressCreateDto addressCreateDto = amenityCreateDto.getAddress();

        Optional<Address> optionalAddressEntity = this.addressService.fetchAddress(addressCreateDto.getPropertyNumber(), addressCreateDto.getStreetName(),
                                                    addressCreateDto.getPostalCode(), addressCreateDto.getCity(),
                                                    addressCreateDto.getCountry());
        Address address = optionalAddressEntity.map( addressEntity -> {
            var fetchedAmenity = this.fetchAmenity(amenityCreateDto.getType(), amenityCreateDto.getName(), addressEntity.getId());
            fetchedAmenity.ifPresent(amenity -> {
                throw new AmenityAlreadyExistException("Amenity already exist .");
            });
            return addressEntity;
        })
                .orElseGet(() -> {
                    Address addressToCreate = new Address(amenityCreateDto.getAddress());
                    var addressDto = addressService.create(addressToCreate)
                            .orElseThrow(() -> new AddressCreationFailedException("Failed to create address"));
                    return new Address(addressDto);
                });
        Amenity amenity = new Amenity(amenityCreateDto);
        amenity.setAddress(address);
        Amenity amenitySaved = amenityRepository.save(amenity);
        return new AmenityDto(amenitySaved);
    }

    @Override
    public void deleteAmenityById(Long amenityId) throws AmenityNotFoundException {
        var amenityFound = amenityRepository.findById(amenityId)
                .orElseThrow(() -> new AmenityNotFoundException("Property with ID (" + amenityId + ") not found ."));
        amenityRepository.delete(amenityFound);
    }

    @Override
    public AmenityDto update(Long id, AmenityUpdateDto propertyUpdateDto) throws AmenityNotFoundException {
        Amenity amenityFetched = amenityRepository.findById(id)
                .orElseThrow(() -> new AmenityNotFoundException("Amenity with ID (" + id + ") not found ."));
        AddressCreateDto addressCreateDtoFetched = new AddressCreateDto(new AddressDto(amenityFetched.getAddress()));
        if( !addressCreateDtoFetched.equals(propertyUpdateDto.getAddress()) ) { //si on a une address à update
            // On créer une nouvelle address
            var address = new Address(propertyUpdateDto.getAddress());
            //TODO Should return an AddressDto
            var addressDto = addressService.create(address)
                    .orElseThrow(() -> new AddressCreationFailedException("Create address failed"));
            amenityFetched.setAddress(new Address(addressDto));
        }

        BeanUtils.copyProperties(propertyUpdateDto, amenityFetched);
        Amenity amenity = amenityRepository.save(amenityFetched);

        return new AmenityDto(amenity);
    }

    @Override
    public List<AmenityDto> findAllAmenitiesByRange(double latitude,
                                                    double longitude,
                                                    double distance) {
        /**
         * 1° Latitude = 111.11km
         * 1° Longitude = 111.11km x cos(Latitude)
         */
        var factorLatitudeTokKm = 111.11;
        var factorLongitudeToKm = 111.11 * Math.cos(Math.toRadians(latitude));
        var amenities = amenityRepository.findByRange(latitude,longitude, distance,
                                                        factorLatitudeTokKm, factorLongitudeToKm);
        return amenities.stream()
                .map(AmenityDto::new)
                .collect(Collectors.toList());
    }
}
