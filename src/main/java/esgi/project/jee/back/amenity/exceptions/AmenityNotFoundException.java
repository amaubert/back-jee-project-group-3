package esgi.project.jee.back.amenity.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class AmenityNotFoundException extends ResourceNotFoundException {
    public AmenityNotFoundException(String message) {
        super(message);
    }
}
