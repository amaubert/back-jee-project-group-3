package esgi.project.jee.back.config;

import esgi.project.jee.back.security.definitions.Roles;
import esgi.project.jee.back.token.definitions.services.ITokenService;
import esgi.project.jee.back.share.Routes;
import esgi.project.jee.back.security.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static esgi.project.jee.back.share.Routes.SWAGGER_ROUTES;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final ITokenService tokenService;

    @Autowired
    public SecurityConfig(ITokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(SWAGGER_ROUTES);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http    .cors()
                .and()
                .csrf().disable() // We don't need CSRF
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, Routes.PUBLIC_ROUTES_GET).permitAll() // dont authenticate this particular request
                .antMatchers(HttpMethod.POST, Routes.PUBLIC_ROUTES_POST).permitAll()
                .antMatchers(HttpMethod.PUT, Routes.PUBLIC_ROUTES_PUT).permitAll()
                .antMatchers(HttpMethod.DELETE, Routes.PUBLIC_ROUTES_DELETE).permitAll()
                .antMatchers(HttpMethod.GET, Routes.ADMIN_ROUTES_GET).hasRole(Roles.Admin.name())
                .antMatchers(HttpMethod.POST, Routes.ADMIN_ROUTES_POST).hasRole(Roles.Admin.name())
                .antMatchers(HttpMethod.PUT, Routes.ADMIN_ROUTES_PUT).hasRole(Roles.Admin.name())
                .antMatchers(HttpMethod.DELETE, Routes.ADMIN_ROUTES_DELETE).hasRole(Roles.Admin.name())
                .anyRequest().authenticated() // all other requests need to be authenticated
            .and()
                .addFilterBefore(new JwtFilter(tokenService), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
