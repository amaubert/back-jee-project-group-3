package esgi.project.jee.back.property.definition.dtos;

import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

//TODO this DTO should have the address propertie => AddressCreateDto address
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertyCreateDto {
    @NotNull(message = "{price.null}")
    private Double price;

    @NotNull(message = "{transaction-type.null}")
    private TransactionType transactionType;

    @NotNull(message = "{surface.null}")
    private Double surface;

    @NotNull(message = "{rooms.null}")
    private Integer rooms;

    @NotNull
    @Valid
    private AddressCreateDto address;

    @NotNull(message = "{is-available.null}")
    private Boolean isAvailable;

    public PropertyCreateDto(PropertyDto propertyDto) {
        this.price = propertyDto.getPrice();
        this.transactionType = propertyDto.getTransactionType();
        this.surface = propertyDto.getSurface();
        this.rooms = propertyDto.getRooms();
        this.address = new AddressCreateDto(propertyDto.getAddress());
        this.isAvailable = propertyDto.getIsAvailable();
    }
}
