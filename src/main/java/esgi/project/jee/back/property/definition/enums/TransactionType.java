package esgi.project.jee.back.property.definition.enums;

public enum TransactionType {
    LEASING,
    SALE
}
