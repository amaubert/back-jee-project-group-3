package esgi.project.jee.back.property.definition.dtos;

import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.property.entities.Property;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertyDto {
    @NotNull
    private Long id;
    @NotNull
    private Double price;
    @NotNull
    private TransactionType transactionType;
    @NotNull
    private Double surface;
    @NotNull
    private Integer rooms;
    @NotNull
    private AddressDto address;
    @NotNull
    private Boolean isAvailable;
    private UserResponseDto purchaser;

    public PropertyDto( @NotNull Property property) {
        this.id = property.getId();
        this.price = property.getPrice();
        this.transactionType = property.getTransactionType();
        this.surface = property.getSurface();
        this.rooms = property.getRooms();
        this.address = new AddressDto(property.getAddress());
        this.isAvailable = property.getIsAvailable();
        if(  property.getPurchaser() != null) {
            this.purchaser = new UserResponseDto(property.getPurchaser());
        }
    }
}
