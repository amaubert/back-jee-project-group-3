package esgi.project.jee.back.property.repositories;

import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.property.entities.Property;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PropertyRepository extends CrudRepository<Property, Long>, QueryByExampleExecutor<Property> {
    Optional<Property> findByPriceAndSurfaceAndRoomsAndTransactionTypeAndAddressId(Double price,
                                                                                   Double surface,
                                                                                   int rooms,
                                                                                   TransactionType transactionType,
                                                                                   Long addressId
    );

    List<Property> findAll();

}
