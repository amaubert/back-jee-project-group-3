package esgi.project.jee.back.property.controllers;

import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.share.Routes;
import esgi.project.jee.back.property.definition.dtos.PropertyCreateDto;
import esgi.project.jee.back.property.definition.dtos.PropertyDto;
import esgi.project.jee.back.property.definition.dtos.PropertyUpdateDto;
import esgi.project.jee.back.property.definition.IPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static esgi.project.jee.back.share.Routes.Property.*;

@RestController
@RequestMapping( path = Routes.Property.Prefix ,produces = MediaType.APPLICATION_JSON_VALUE)
public class PropertyController {

    private final IPropertyService propertyService;

    @Autowired
    public PropertyController(IPropertyService propertyService) {
        this.propertyService = propertyService;
    }


    @PostMapping()
    public ResponseEntity<PropertyDto> create(@RequestBody @Valid PropertyCreateDto propertyCreateDto,
                                       UriComponentsBuilder uriBuilder) {
        var propertyDto = propertyService.create(propertyCreateDto);
        URI uri = uriBuilder.path(Routes.Property.completeUri(Consult))
                .buildAndExpand(propertyDto.getId()).toUri();
        return ResponseEntity.created(uri).body(propertyDto);
    }

    @GetMapping
    public ResponseEntity<List<PropertyDto>> list(Principal principal){
        List<PropertyDto> propertyDtoList;
        if( principal != null) {
            propertyDtoList = propertyService.findAllPropertiesWithDiscount(principal);
        } else {
            propertyDtoList = propertyService.findAllProperties();
        }
        return ResponseEntity.ok().body(propertyDtoList);
    }

    @GetMapping(Consult)
    public ResponseEntity<PropertyDto> getById(Principal principal, @PathVariable("propertyId") Long id){
        PropertyDto propertyDto;
        if( principal != null){
            propertyDto = propertyService.findPropertyByIdWithDiscount(principal, id);
        } else {
            propertyDto = propertyService.findPropertyById(id);
        }
        return ResponseEntity.ok(propertyDto);
    }

    @DeleteMapping(Routes.Property.Delete)
    public ResponseEntity<?> delete(@PathVariable("propertyId") Long id) {
        propertyService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(Update)
    public ResponseEntity<PropertyDto> update( @PathVariable("propertyId") Long id,
                                     @RequestBody @Valid PropertyUpdateDto propertyUpdateDto){
        var propertyDto = propertyService.update(id, propertyUpdateDto);
        return ResponseEntity.ok(propertyDto);
    }

    @PutMapping(Buy)
    public ResponseEntity<PropertyDto> buy( @PathVariable("propertyId") Long id,
                                            Principal principal){
        var propertyDto = propertyService.buy(principal, id);
        return ResponseEntity.ok(propertyDto);
    }

    @GetMapping(Amenities)
    public ResponseEntity<List<AmenityDto>> amenities( @PathVariable("propertyId") Long id,
                                                       @RequestParam("distance") Double distance) {
        if( distance == null || distance < 3 ) {
            distance = 15.0;
        }
        List<AmenityDto> amenitiesDto = propertyService.getAllAmenitiesInRange(id, distance);
        return ResponseEntity.ok().body(amenitiesDto);
    }
}
