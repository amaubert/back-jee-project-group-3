package esgi.project.jee.back.property.definition.dtos;

import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertyUpdateDto {
    @NotNull
    private Double price;
    @NotNull
    private TransactionType transactionType;
    @NotNull
    private Double surface;
    @NotNull
    private Integer rooms;
    @NotNull
    @Valid
    private AddressCreateDto address;
    @NotNull
    private Boolean isAvailable;

    private Long purchaserId;

    public PropertyUpdateDto(PropertyDto propertyDto) {
        this.price = propertyDto.getPrice();
        this.transactionType = propertyDto.getTransactionType();
        this.surface = propertyDto.getPrice();
        this.rooms = propertyDto.getRooms();
        this.address = new AddressCreateDto(propertyDto.getAddress());
        this.isAvailable = propertyDto.getIsAvailable();
        if(propertyDto.getPurchaser() != null  ) {
            this.purchaserId = propertyDto.getPurchaser().getId();
        }

    }
}
