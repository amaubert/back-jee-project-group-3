package esgi.project.jee.back.property.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class PropertyNotFoundException extends ResourceNotFoundException {
    public PropertyNotFoundException(String message) {
        super(message);
    }
}
