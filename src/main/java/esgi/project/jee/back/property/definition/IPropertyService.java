package esgi.project.jee.back.property.definition;

import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.property.definition.dtos.PropertyCreateDto;
import esgi.project.jee.back.property.definition.dtos.PropertyDto;
import esgi.project.jee.back.property.definition.dtos.PropertyUpdateDto;
import esgi.project.jee.back.property.entities.Property;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface IPropertyService {
    Optional<Property> fetchProperty(Double price, Double surface, int rooms, TransactionType transactionType, Long id);
    PropertyDto findPropertyById(Long id);
    PropertyDto findPropertyByIdWithDiscount(Principal principal, Long id);
    List<PropertyDto> findAllPropertiesWithDiscount(Principal principal);
    List<PropertyDto> findAllProperties();
    void delete(Long id);
    PropertyDto create(PropertyCreateDto propertyCreateDto);
    PropertyDto update(Long id, PropertyUpdateDto propertyUpdateDto);
    PropertyDto buy(Principal principal, Long id);
    List<AmenityDto> getAllAmenitiesInRange(Long id, double distance);
}
