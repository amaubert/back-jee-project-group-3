package esgi.project.jee.back.property.services;

import esgi.project.jee.back.amenity.definitions.IAmenityService;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.sponsorship.definitions.services.ISponsorshipService;
import esgi.project.jee.back.user.definitions.services.IUserService;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.property.definition.dtos.PropertyCreateDto;
import esgi.project.jee.back.property.definition.dtos.PropertyDto;
import esgi.project.jee.back.property.definition.dtos.PropertyUpdateDto;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.property.definition.IPropertyService;
import esgi.project.jee.back.address.exceptions.AddressCreateFailedException;
import esgi.project.jee.back.property.exceptions.PropertyAlreadyExistException;
import esgi.project.jee.back.property.exceptions.PropertyNotFoundException;
import esgi.project.jee.back.property.repositories.PropertyRepository;
import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.property.entities.Property;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PropertyService implements IPropertyService {

    private final PropertyRepository propertyRepository ;
    private final IAddressService addressService;
    private final IUserService userService;
    private final ISponsorshipService sponsorshipService;
    private final IAmenityService amenityService;

    private static final double DISCOUNT_SPONSORED_USER = 0.05;

    private final Double MAX_KM = 15.0;

    @Autowired
    public PropertyService(PropertyRepository propertyRepository,
                           IAddressService addressService,
                           IUserService userService,
                           ISponsorshipService sponsorshipService,
                           IAmenityService amenityService) {
        this.propertyRepository = propertyRepository;
        this.addressService = addressService;
        this.userService = userService;
        this.sponsorshipService = sponsorshipService;
        this.amenityService = amenityService;
    }

    @Override
    public Optional<Property> fetchProperty(Double price, Double surface, int rooms, TransactionType transactionType, Long addressId) {
        return this.propertyRepository.findByPriceAndSurfaceAndRoomsAndTransactionTypeAndAddressId(
                price,
                surface,
                rooms,
                transactionType,
                addressId);
    }

    @Override
    public PropertyDto findPropertyById(Long id) {
        var property =  this.propertyRepository.findById(id)
                                .orElseThrow(() -> new PropertyNotFoundException("Property with ID (" +
                                                                                    id + ") not found ."));
        return new PropertyDto(property);
    }

    @Override
    public PropertyDto findPropertyByIdWithDiscount(Principal principal, Long id) {
        var property =  this.propertyRepository.findById(id)
                .orElseThrow(() -> new PropertyNotFoundException("Property with ID (" +
                        id + ") not found ."));

        var userDto = userService.findByAccountLogin(principal.getName());

        if( sponsorshipService.isSponsored(userDto) ){
            property.setPrice(property.getPrice() * 0.95);
        }
        return new PropertyDto(property);
    }

    @Override
    public List<PropertyDto> findAllPropertiesWithDiscount(Principal principal) {
        var properties = findAllProperties();
        var userDto = userService.findByAccountLogin(principal.getName());
        if( sponsorshipService.isSponsored(userDto) ) {
            properties.forEach(propertyDto ->
                                propertyDto.setPrice(propertyDto.getPrice() * (1.0 - DISCOUNT_SPONSORED_USER)));
        }
        return properties;
    }

    public List<PropertyDto> findAllProperties() {
            return propertyRepository.findAll().stream()
                                        .map(PropertyDto::new)
                                        .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        var propertyFound = propertyRepository.findById(id)
                                              .orElseThrow(() ->
                                                        new PropertyNotFoundException("Property with ID (" +
                                                                                        id + ") not found .")
                                              );
        propertyRepository.delete(propertyFound);
    }

    @Override
    public PropertyDto create(PropertyCreateDto propertyCreateDto) {
        var addressCreateDto = propertyCreateDto.getAddress();
        //TODO addressService.fetchAddress() parameter should be an AddressFetchDto
        //TODO addressService.fetchAddress() should return an AddressDto
        var optionalAddressEntity = this.addressService.fetchAddress(addressCreateDto.getPropertyNumber(), addressCreateDto.getStreetName(),
                                        addressCreateDto.getPostalCode(), addressCreateDto.getCity(),
                                        addressCreateDto.getCountry());
        var address = optionalAddressEntity.map( addressEntity -> {
                    //TODO this.fetchProperty() parameter should be a PropertyFetchDto
                    var fetchedProperty = this.fetchProperty(propertyCreateDto.getPrice(), propertyCreateDto.getSurface(),
                                                             propertyCreateDto.getRooms(), propertyCreateDto.getTransactionType(),
                                                             addressEntity.getId());
                    fetchedProperty.ifPresent(property -> {
                        throw new PropertyAlreadyExistException("Property already exist .");
                    });
                    return addressEntity;
                })
                .orElseGet(() -> {
                    //TODO addressService.create() parameter should be an AddressCreateDto
                    //TODO addressService.create() should return an AddressDto

                    Address addressToCreate = new Address(propertyCreateDto.getAddress());
                    var addressDto = addressService.create(addressToCreate)
                                        .orElseThrow(() -> new AddressCreateFailedException("Failed to create address"));
                    return new Address(addressDto);
                });
        var property = new Property(propertyCreateDto);
        property.setAddress(address);
        var propertySaved = propertyRepository.save(property);
        return new PropertyDto(propertySaved);
    }

    @Override
    public PropertyDto update(Long id, PropertyUpdateDto propertyUpdateDto) {
        var propertyFetched = propertyRepository.findById(id)
                                            .orElseThrow(() -> new PropertyNotFoundException("Property with ID (" +
                                                                                             id + ") not found ."));
        var addressCreateDtoFetched = new AddressCreateDto(new AddressDto(propertyFetched.getAddress()));
        if( !addressCreateDtoFetched.equals(propertyUpdateDto.getAddress()) ) { //si on a une address à update
            // On créer une nouvelle address
            var address = new Address(propertyUpdateDto.getAddress());
            //TODO Should return an AddressDto
            var addressDto = addressService.create(address)
                                        .orElseThrow(() -> new AddressCreateFailedException("Create addres failed"));
            propertyFetched.setAddress(new Address(addressDto));
        }

        if( propertyUpdateDto.getPurchaserId() != null) {
            var userDto = userService.findById(propertyUpdateDto.getPurchaserId());
            propertyFetched.setPurchaser(new User(userDto));
        }

        BeanUtils.copyProperties(propertyUpdateDto, propertyFetched);
        var property = propertyRepository.save(propertyFetched);

        return new PropertyDto(property);
    }

    @Override
    public PropertyDto buy(Principal principal, Long id) {
        var property =  this.propertyRepository.findById(id)
                .orElseThrow(() -> new PropertyNotFoundException("Property with ID (" +
                        id + ") not found ."));

        var userDto = userService.findByAccountLogin(principal.getName());
        property.setIsAvailable(false);
        property.setPurchaser(new User(userDto));
        property = propertyRepository.save(property);
        return new PropertyDto(property);

    }

    @Override
    public List<AmenityDto> getAllAmenitiesInRange(Long id, double distance) {
        Property property =  this.propertyRepository.findById(id)
                .orElseThrow(() -> new PropertyNotFoundException("Property with ID (" +
                        id + ") not found ."));

        var address = property.getAddress();

        return amenityService.findAllAmenitiesByRange(address.getLatitude(), address.getLongitude(), distance);
    }

    private boolean isInPerimeter(Property property, AmenityDto amenityDto) {
        // ACOS( (SIN(RADIANS(propLat)) * SIN(RADIANS(amLat))) + COS(RADIANS(propLat)) * COS(RADIANS(amLat)) * COS(RADIANS(propLong-amLong))) * 6371
        Double propLat = property.getAddress().getLatitude();
        Double propLong = property.getAddress().getLongitude();
        Double amLat = amenityDto.getAddress().getLatitude();
        Double amLong = amenityDto.getAddress().getLongitude();

        Double tmp = Math.sin(Math.toRadians(propLat)) * Math.sin(Math.toRadians(amLat));
        Double tmp2 = Math.cos(Math.toRadians(propLat)) * Math.cos(Math.toRadians(amLat)) * Math.cos(Math.toRadians(propLong - amLong));
        Double result = Math.acos(tmp + tmp2) * 6371;

        if(result <= MAX_KM)
            return true;
        return false;
    }
}
