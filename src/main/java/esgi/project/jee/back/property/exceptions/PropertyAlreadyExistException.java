package esgi.project.jee.back.property.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class PropertyAlreadyExistException extends ResourceAlreadyExistException {
    public PropertyAlreadyExistException(String message) {
        super(message);
    }
}
