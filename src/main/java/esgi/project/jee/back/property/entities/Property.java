package esgi.project.jee.back.property.entities;

import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.property.definition.dtos.PropertyCreateDto;
import esgi.project.jee.back.property.definition.dtos.PropertyDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "Property")
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Property extends BaseEntity {
    @Column(columnDefinition = "Decimal(10,2)")
    private Double price;
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;
    @Column(columnDefinition = "Decimal(10,2)")
    private Double surface;
    @Column(columnDefinition = "integer")
    private Integer rooms;
    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(
            foreignKey = @ForeignKey(name = "fk_property_address",
                    value = ConstraintMode.CONSTRAINT))
    private Address address;
    @Column(columnDefinition = "boolean default true")
    private Boolean isAvailable;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(
            foreignKey = @ForeignKey(name = "fk_property_user",
                    value = ConstraintMode.CONSTRAINT))
    private User purchaser;

    public Property() {
    }

    public Property(PropertyCreateDto propertyCreateDto) {
        this.price = propertyCreateDto.getPrice();
        this.transactionType = propertyCreateDto.getTransactionType();
        this.surface = propertyCreateDto.getSurface();
        this.rooms = propertyCreateDto.getRooms();
        this.isAvailable = propertyCreateDto.getIsAvailable();
    }

    public Property(PropertyDto propertyDto) {
        this.id = propertyDto.getId();
        this.price = propertyDto.getPrice();
        this.transactionType = propertyDto.getTransactionType();
        this.surface = propertyDto.getSurface();
        this.rooms = propertyDto.getRooms();
        this.address = new Address(propertyDto.getAddress());
        this.isAvailable = propertyDto.getIsAvailable();
    }

}
