package esgi.project.jee.back.security.definitions;


import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface ITokenProvider {
    String createToken(Authentication authentication);
    Authentication getAuthentication(String jwtToken);
    boolean validateToken(String jwtToken);
    boolean isNotExpired(String jwtToken);
}
