package esgi.project.jee.back.security.definitions;

public enum Roles {
    User("User"),
    Admin("Admin");

    private String role;

    Roles(String role) {
        this.role = role;
    }
}
