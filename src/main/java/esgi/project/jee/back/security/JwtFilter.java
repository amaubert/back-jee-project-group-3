package esgi.project.jee.back.security;

import esgi.project.jee.back.token.definitions.services.ITokenService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;

public class JwtFilter extends GenericFilterBean {

    private final ITokenService tokenService;

    public JwtFilter(ITokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        var httpRequest = (HttpServletRequest)servletRequest;
        String jwtToken = TokenProvider.resolveToken(httpRequest);

        if (hasText(jwtToken) && tokenService.validateToken(jwtToken)) {
            Authentication authentication = tokenService.getTokenProvider().getAuthentication(jwtToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
