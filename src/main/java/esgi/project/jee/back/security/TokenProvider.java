package esgi.project.jee.back.security;

import esgi.project.jee.back.security.definitions.ITokenProvider;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.util.StringUtils.hasText;

@Component
@Slf4j
public class TokenProvider implements ITokenProvider {

    private final long tokenValidityInMilliSeconds;
    private final byte[] secretKey;
    public static final String Token_Prefix = "Bearer ";
    private static final String AUTHORITIES_KEY = "auth";

    public TokenProvider(@Value("${security.jwt.token.secret-key}") String secretKey,
                         @Value("${jwt.token.expire-length:1800000 }") Long tokenValidityInMilliSeconds ) {
        this.tokenValidityInMilliSeconds = tokenValidityInMilliSeconds ;
        this.secretKey = secretKey.getBytes();
    }
    @Override
    public String createToken(Authentication authentication) {
        String authorities = authentication.getAuthorities().stream()
                                                .map(GrantedAuthority::getAuthority)
                                                .collect(Collectors.joining(","));
        return Jwts.builder()
                    .setSubject(authentication.getName())
                    .claim(AUTHORITIES_KEY, authorities)
                    .setExpiration(new Date(System.currentTimeMillis() + tokenValidityInMilliSeconds))
                    .signWith(SignatureAlgorithm.HS256, secretKey)
                    .compact();
    }

    @Override
    public Authentication getAuthentication(String jwtToken) {
        Claims claims = parseToken(jwtToken).getBody();
        var authorities = Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                                    .map(SimpleGrantedAuthority::new)
                                    .collect(Collectors.toList());
        User principal = new User(claims.getSubject(), "", authorities);
        return new UsernamePasswordAuthenticationToken(principal, jwtToken, authorities);
    }

    @Override
    public boolean validateToken(String jwtToken) {
        try {
            parseToken(jwtToken);

            return isNotExpired(jwtToken);
        } catch (JwtException | IllegalArgumentException exception) {
            log.info("Invalid Jwt Token");
            return false;
        }
    }
    @Override
    public boolean isNotExpired(String jwtToken) {
        Date expirationDate = parseToken(jwtToken).getBody().getExpiration();
        Date now = new Date(System.currentTimeMillis());
        return expirationDate.after(now);
    }

    private Jws<Claims> parseToken(String jwtToken) {
        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(jwtToken);
    }

   static public String resolveToken(HttpServletRequest httpRequest) {
        String bearerToken = httpRequest.getHeader(AUTHORIZATION);
        if (hasText(bearerToken) && bearerToken.startsWith(Token_Prefix)) {
            return bearerToken.substring(Token_Prefix.length());
        }
        return null;
    }
}
