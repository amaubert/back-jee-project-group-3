package esgi.project.jee.back.share.plateforms.gps.definitions;

import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.AddressAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.GeometryAPIDto;
import esgi.project.jee.back.address.entities.Address;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface IAPIAddress {
    public ResponseEntity<AddressAPIDto> getCoordinates(String address, int postCode ) throws IOException;
    public GeometryAPIDto getCoordinates(String address) throws IOException;
    public String stringifyAddress(Address address);
}
