package esgi.project.jee.back.share.plateforms.gps.definitions.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class FeaturesAPIDto {
    private GeometryAPIDto geometry;

    public FeaturesAPIDto(){

    }

    public FeaturesAPIDto(GeometryAPIDto geometry){
        this.geometry = geometry;
    }
}
