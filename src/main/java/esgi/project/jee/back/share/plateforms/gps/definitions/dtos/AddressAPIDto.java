package esgi.project.jee.back.share.plateforms.gps.definitions.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class AddressAPIDto {

    private FeaturesAPIDto[] features;

    public AddressAPIDto(){

    }

    public AddressAPIDto(FeaturesAPIDto[] features){
        this.features = features;
    }
}
