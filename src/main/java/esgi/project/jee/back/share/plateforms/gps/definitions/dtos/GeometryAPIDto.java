package esgi.project.jee.back.share.plateforms.gps.definitions.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class GeometryAPIDto {

    private String type;
    private Double[] coordinates;



    public GeometryAPIDto(){

    }

    public GeometryAPIDto(String type, Double[] coordinates){
        this.type = type;
        this.coordinates = coordinates;
    }
}
