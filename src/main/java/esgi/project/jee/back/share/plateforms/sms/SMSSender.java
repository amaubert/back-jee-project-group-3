package esgi.project.jee.back.share.plateforms.sms;

import esgi.project.jee.back.share.plateforms.sms.definitions.dtos.ListSMSResponseDto;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import esgi.project.jee.back.share.plateforms.sms.exceptions.SendSmsFailedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Duration;

@Component
public class SMSSender implements ISMSSender {
    private final String apiKey;
    private final String baseUrl;
    private final RestTemplate restTemplate;

    public SMSSender(@Value("${api.sms.apiKey}") String apiKey,
                     @Value("${api.sms.baseUrl}") String baseUrl,
                     RestTemplateBuilder restTemplateBuilder) {
        this.apiKey = apiKey;
        this.baseUrl = baseUrl;
        this.restTemplate = restTemplateBuilder
                                .setConnectTimeout(Duration.ofSeconds(3))
                                .setReadTimeout(Duration.ofSeconds(20))
                                .build();
    }

    public ResponseEntity<ListSMSResponseDto> send(String to, String content) {
        var uri = UriComponentsBuilder.fromUriString(baseUrl)
                .queryParam("apiKey", apiKey)
                .queryParam("to", to)
                .queryParam("content", content)
                .build()
                .toString();
        ResponseEntity<ListSMSResponseDto> response;
        try{
            response = restTemplate.getForEntity(uri, ListSMSResponseDto.class);
        } catch (ResourceAccessException exception) {
            throw new SendSmsFailedException(exception.getCause().getMessage());
        }
        return  response;
    }
}
