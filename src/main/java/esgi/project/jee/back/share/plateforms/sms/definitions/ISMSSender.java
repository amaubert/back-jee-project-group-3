package esgi.project.jee.back.share.plateforms.sms.definitions;

import esgi.project.jee.back.share.plateforms.sms.definitions.dtos.ListSMSResponseDto;
import org.springframework.http.ResponseEntity;

public interface ISMSSender {
    ResponseEntity<ListSMSResponseDto> send(String to, String content);
}
