package esgi.project.jee.back.share.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    /*private final static Pattern SPECIAL_CHARACTER_REGEX = Pattern.compile("[&+-/\"\'()<>]+");
    private final static Pattern UPPERCASE_REGEX = Pattern.compile("[A-Z]+");
    private final static Pattern NUMBER_REGEX = Pattern.compile("[\\d]+");*/

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        /*return !isEmpty(s) &&
                (s.length() >= 8 && s.length() <= 30) &&
                NUMBER_REGEX.matcher(s).find() &&
                UPPERCASE_REGEX.matcher(s).find() &&
                SPECIAL_CHARACTER_REGEX.matcher(s).find();*/
        return s.length() >= 8 && s.length() <= 30;
    }
}
