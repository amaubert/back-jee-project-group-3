package esgi.project.jee.back.share.plateforms.sms.definitions.dtos;

import lombok.Data;

import java.util.List;

@Data
public class ListSMSResponseDto {
    private List<SMSResponseDto> messages;
}
