package esgi.project.jee.back.share.plateforms.sms.definitions.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSResponseDto {
    public String apiMessageId;
    public Boolean accepted;
    public String to;
    public String errorCode;
    public String error;
    public String errorDescription;
}
