package esgi.project.jee.back.share.plateforms.sms.exceptions;

public class SendSmsFailedException extends RuntimeException {
    public SendSmsFailedException(String message) {
        super(message);
    }
}
