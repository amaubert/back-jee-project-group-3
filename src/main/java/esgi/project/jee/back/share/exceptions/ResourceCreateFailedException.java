package esgi.project.jee.back.share.exceptions;

public class ResourceCreateFailedException extends RuntimeException {
    public ResourceCreateFailedException(String message) {
        super(message);
    }
}
