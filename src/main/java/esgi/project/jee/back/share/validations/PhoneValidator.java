package esgi.project.jee.back.share.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PhoneValidator implements ConstraintValidator<Phone, String> {
    private final Pattern pattern = Pattern.compile("^33[1-9]([-. ]?[0-9]{2}){4}$");
    @Override
    public boolean isValid(String s,
                           ConstraintValidatorContext constraintValidatorContext) {
        final var matcher = pattern.matcher(s);
        return matcher.find();
    }
}

