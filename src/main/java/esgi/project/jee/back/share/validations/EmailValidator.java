package esgi.project.jee.back.share.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

import static org.springframework.util.StringUtils.hasText;

public class EmailValidator implements ConstraintValidator<Email, String> {
    private final Pattern pattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*[.]{1}[a-zA-Z0-9]{0,20}$");
    @Override
    public boolean isValid(String s,
                           ConstraintValidatorContext constraintValidatorContext) {
        if(!hasText(s)){
            return false;
        }
        final var matcher = pattern.matcher(s);
        return matcher.find();
    }
}
