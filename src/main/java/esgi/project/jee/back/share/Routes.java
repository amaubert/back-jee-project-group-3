package esgi.project.jee.back.share;

public class Routes {

    static public class Hello {
        public static final String Prefix = "/hello";

    }
    static public class Account {
        public static final String Prefix = "/account";
        public static final String Login = "/login";
        public static final String Me = "/me";
        public static final String Subscribe = "/subscribe";
        public static final String Activate = "/{accountId}/activate";
        public static final String Logout = "/logout";
        public static final String ResendCode = "/{accountId}/resend-code";

        public static String completeUri(String endpoint){
            return Prefix + endpoint;
        }
    }

    static public class User {
        public static final String Prefix = "/user";
        public static final String Sponsor = "/sponsor";

        public static String completeUri(String endpoint){
            return Prefix + endpoint;
        }
    }

    static public class Property {
        public static final String Prefix = "/properties";
        public static final String Consult = "/{propertyId}";
        public static final String Delete = "/{propertyId}";
        public static final String Update = "/{propertyId}";
        public static final String Buy = "/{propertyId}/buy";
        public static final String Amenities = "/{propertyId}/amenities";

        public static String completeUri(String endpoint){
            return Prefix + endpoint;
        }
    }

    static public class Amenity {
        public static final String Prefix = "/amenities";
        public static final String Delete = "/{amenityId}";
        public static final String Update = "/{amenityId}";

        public static String completeUri(String endpoint){
            return Prefix + endpoint;
        }
    }
    static public class Distance {
        public static final String Prefix = "/distance";
        public static final String Distance2Points = "/property/{propertyId}";

        public static String completeUri(String endpoint){
            return Prefix + endpoint;
        }
    }

    public static final String[] SWAGGER_ROUTES = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    public static final String[] PUBLIC_ROUTES_GET = {
            Property.Prefix,
            Property.completeUri(Property.Consult),
            Hello.Prefix
    };

    public static final String[] PUBLIC_ROUTES_POST = {
            Account.completeUri(Account.Subscribe),
            Account.completeUri(Account.Login),
            Account.completeUri(Account.ResendCode)
    };

    public static final String[] PUBLIC_ROUTES_PUT = {
            Account.completeUri(Account.Activate)
    };

    public static final String[] PUBLIC_ROUTES_DELETE = {

    };

    public static final String[] LOGGED_ROUTES_GET = {
            Account.completeUri(Account.Me),
            Distance.completeUri(Distance.Distance2Points),
            Property.completeUri(Property.Amenities)
    };

    public static final String[] LOGGED_ROUTES_POST = {
            Account.completeUri(Account.Logout)
    };

    public static final String[] LOGGED_ROUTES_PUT = {
            Property.completeUri(Property.Buy),
            Account.completeUri(Account.Me)
    };

    public static final String[] LOGGED_ROUTES_DELETE = {

    };

    public static final String[] ADMIN_ROUTES_GET = {
            Amenity.Prefix
    };

    public static final String[] ADMIN_ROUTES_POST = {
            Property.Prefix,
            Amenity.Prefix,
            User.completeUri(User.Sponsor)
    };

    public static final String[] ADMIN_ROUTES_PUT = {
        Property.completeUri(Property.Update),
        Amenity.completeUri(Amenity.Update)
    };

    public static final String[] ADMIN_ROUTES_DELETE = {
            Property.completeUri(Property.Delete),
            Amenity.completeUri(Amenity.Delete)
    };
}
