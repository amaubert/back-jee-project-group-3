package esgi.project.jee.back.share;

import esgi.project.jee.back.share.defintions.models.ApiError;
import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;
import esgi.project.jee.back.share.exceptions.ResourceCreateFailedException;
import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;
import esgi.project.jee.back.share.plateforms.sms.exceptions.SendSmsFailedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        var servletWebRequest = (ServletWebRequest)request;
        var error = new ApiError(BAD_REQUEST.value(),
                BAD_REQUEST.getReasonPhrase(),
                parseDefaultMessage(exception),
                servletWebRequest.getRequest().getRequestURI());
        return ResponseEntity.status(BAD_REQUEST).body(error);
    }

    private String parseDefaultMessage(MethodArgumentNotValidException exception) {
        StringBuilder message = new StringBuilder();
        var bindingResult = exception.getBindingResult();

        var errors = bindingResult.getFieldErrors();
        if(errors.size() > 1) {
            int count = 1 ;
            for(FieldError error : errors) {
                if(count != errors.size()) {
                    message.append(error.getDefaultMessage())
                            .append(", ");
                } else {
                    message.append(error.getDefaultMessage());
                }
            }
        } else {
            message.append(errors.get(0).getDefaultMessage());
        }



        return message.toString();
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<ApiError> handleDataIntegrityViolationException(DataIntegrityViolationException exception,
                                                                             HttpServletRequest request) {
        String message = exception.getMessage();
        if( exception.getRootCause() != null  ) {
            var cause = exception.getRootCause();
            message = cause.getMessage();
        }
        var error = new ApiError(INTERNAL_SERVER_ERROR.value(),
                INTERNAL_SERVER_ERROR.getReasonPhrase(),
                message,
                request.getRequestURI());
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<ApiError> handleResourceNotFoundException(ResourceNotFoundException exception,
                                                                       HttpServletRequest request) {
        var error = new ApiError(NOT_FOUND.value(),
                NOT_FOUND.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(NOT_FOUND).body(error);
    }

    @ExceptionHandler(ResourceAlreadyExistException.class)
    protected ResponseEntity<ApiError> handleResourceAlreadyExistException(ResourceAlreadyExistException exception,
                                                                           HttpServletRequest request) {
        var error = new ApiError(CONFLICT.value(),
                CONFLICT.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(CONFLICT).body(error);
    }

    @ExceptionHandler(ResourceCreateFailedException.class)
    protected ResponseEntity<ApiError> handleResourceCreateFailedException(ResourceCreateFailedException exception,
                                                                           HttpServletRequest request) {
        var error = new ApiError(INTERNAL_SERVER_ERROR.value(),
                INTERNAL_SERVER_ERROR.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(SendSmsFailedException.class)
    protected ResponseEntity<ApiError> handleSocketTimeoutException(SendSmsFailedException exception,
                                                                    HttpServletRequest request) {
        var error = new ApiError(INTERNAL_SERVER_ERROR.value(),
                INTERNAL_SERVER_ERROR.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(error);

    }

    @ExceptionHandler(AuthenticationException.class)
    protected  ResponseEntity<ApiError> handleAuthenticationException(AuthenticationException exception,
                                                                      HttpServletRequest request) {
        var error = new ApiError(FORBIDDEN.value(),
                FORBIDDEN.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(FORBIDDEN).body(error);
    }
}
