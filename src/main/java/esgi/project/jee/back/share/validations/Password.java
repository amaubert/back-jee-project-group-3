package esgi.project.jee.back.share.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD} )
@Retention( RetentionPolicy.RUNTIME)
public @interface Password {
    String message() default "Invalid password : min 8 characters and max 30 characters.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
