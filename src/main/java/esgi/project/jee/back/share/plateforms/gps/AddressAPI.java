package esgi.project.jee.back.share.plateforms.gps;

import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.AddressAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.GeometryAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.address.entities.Address;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Component
public class AddressAPI implements IAPIAddress {

    private final String baseUrl;
    private final RestTemplate restTemplate;

    public AddressAPI( @Value("${api.address.baseUrl}") String baseUrl){
        this.baseUrl = baseUrl;
        this.restTemplate = new RestTemplate();
    }

    @Override
    public ResponseEntity<AddressAPIDto> getCoordinates(String address, int postCode) throws IOException {

        var uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/search")
                .queryParam("q", address )
                .queryParam("postCode", postCode)
                .queryParam("limit", "1")
                .build()
                .toString();

        return restTemplate.getForEntity(uri, AddressAPIDto.class);
    }

    @Override
    public GeometryAPIDto getCoordinates(String address) throws IOException {
        var uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/search")
                .queryParam("q", address )
                .queryParam("limit", "1")
                .build()
                .toString();

        var response = restTemplate.getForEntity(uri, AddressAPIDto.class).getBody();
        assert response != null;
        return response.getFeatures()[0].getGeometry();
    }

    @Override
    public String stringifyAddress(Address address) {
        return address.getPropertyNumber() + "+"
                + address.getStreetName().replace(" ", "+") + "+"
                + address.getCity();
    }
}
