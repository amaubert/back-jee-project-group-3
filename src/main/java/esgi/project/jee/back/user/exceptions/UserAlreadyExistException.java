package esgi.project.jee.back.user.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceAlreadyExistException;

public class UserAlreadyExistException extends ResourceAlreadyExistException {
    public UserAlreadyExistException(String message) {
        super(message);
    }
}
