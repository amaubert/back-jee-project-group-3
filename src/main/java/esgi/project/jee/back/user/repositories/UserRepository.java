package esgi.project.jee.back.user.repositories;

import esgi.project.jee.back.user.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAll();
    Optional<User> findByAccountId(Long accountId);
    Optional<User> findByEmailOrPhone(String email, String phone);
    Optional<User> findByAccount_Login(String login);
}
