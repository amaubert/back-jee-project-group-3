package esgi.project.jee.back.user.services;

import esgi.project.jee.back.account.defintions.dtos.AccountUpdateDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorshipCreateDto;
import esgi.project.jee.back.sponsorship.definitions.services.ISponsorshipService;
import esgi.project.jee.back.user.definitions.dtos.UserCreateDto;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.definitions.services.IUserService;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.user.exceptions.UserAlreadyExistException;
import esgi.project.jee.back.user.exceptions.UserNotFoundException;
import esgi.project.jee.back.user.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

import static org.springframework.util.StringUtils.hasText;

@Service
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final ISponsorshipService sponsorshipService;

    @Autowired
    public UserService(UserRepository userRepository,
                       ISponsorshipService sponsorshipService) {
        this.userRepository = userRepository;
        this.sponsorshipService = sponsorshipService;
    }

    @Override
    public UserDto create(UserCreateDto userCreateDto) {
        userRepository.findByEmailOrPhone(userCreateDto.getEmail(), userCreateDto.getPhone())
                                            .ifPresent(user -> {
                                                throw new UserAlreadyExistException("User already exist .");
                                            });
        var user = new User(userCreateDto);
        user = userRepository.save(user);

        //if we have a sponsorship code, user should be sponsored with a link to the sponsor
        if (hasText(userCreateDto.getSponsorshipCode())) {
            sponsorshipService.linkSponsorship(userCreateDto.getSponsorshipCode(), user);
        }

        return new UserDto(user);
    }

    @Override
    public UserDto findByAccountId(Long accountId) {
        var user = userRepository.findByAccountId(accountId)
                                        .orElseThrow(() -> new UserNotFoundException("User with accountId ("+
                                                                                     accountId + ") not found ."));
        return new UserDto(user);
    }

    @Override
    public UserDto findById(Long userId) {
        var user = userRepository.findById(userId)
                        .orElseThrow(() -> new UserNotFoundException("User with ID ("+ userId +") not found ."));
        return new UserDto(user);
    }

    @Override
    public UserDto findByAccountLogin(String accountLogin) {
        var user = userRepository.findByAccount_Login(accountLogin)
                .orElseThrow(() -> new UserNotFoundException("Account with a login ("+
                        accountLogin + ") not found ."));
        return new UserDto(user);
    }

    @Override
    public SponsorShipDto createSponsorship(SponsorshipCreateDto sponsorshipCreateDto, Principal principal) {
        var user = userRepository.findByAccount_Login(principal.getName())
                            .orElseThrow(() -> new UserNotFoundException("User with a login ("+
                                                                        principal.getName()+ ") not found ."));
        return sponsorshipService.createSponsorshipCode(user, sponsorshipCreateDto.getSendTo());
    }

    @Override
    public UserDto update(AccountUpdateDto accountUpdateDto, String accountLogin) {
        var user = userRepository.findByAccount_Login(accountLogin)
                            .orElseThrow(() -> new UserNotFoundException("user with account login ("+
                                                                    accountLogin +") not found ."));
        updateUser(user, accountUpdateDto);
        user = userRepository.save(user);
        return new UserDto(user);
    }

    private void updateUser(User user, AccountUpdateDto accountUpdateDto) {
        if( accountUpdateDto.getFirstName() != null &&
            !user.getFirstName().equals(accountUpdateDto.getFirstName())) {
            user.setFirstName(accountUpdateDto.getFirstName());
        }
        if( accountUpdateDto.getLastName() != null &&
            !user.getLastName().equals(accountUpdateDto.getLastName())) {
            user.setLastName(accountUpdateDto.getLastName());
        }
        if( accountUpdateDto.getEmail() != null &&
            !user.getEmail().equals(accountUpdateDto.getEmail())) {
            user.setEmail(accountUpdateDto.getEmail());
        }
        if( accountUpdateDto.getPhone() != null &&
            !user.getPhone().equals(accountUpdateDto.getPhone())) {
            user.setPhone(accountUpdateDto.getPhone());
        }
    }
}
