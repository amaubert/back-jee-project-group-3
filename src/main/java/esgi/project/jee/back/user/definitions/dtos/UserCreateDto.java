package esgi.project.jee.back.user.definitions.dtos;

import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.account.defintions.dtos.AccountSubscribeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto {
    private String lastName;
    private String firstName;
    private String phone;
    private String email;
    private AccountDto account;
    private String sponsorshipCode;

    public UserCreateDto(AccountSubscribeDto accountSubscribeDto, AccountDto account) {
        this.lastName = accountSubscribeDto.getLastName();
        this.firstName = accountSubscribeDto.getFirstName();
        this.email = accountSubscribeDto.getEmail();
        this.phone = accountSubscribeDto.getPhone();
        this.account = account;
        this.sponsorshipCode = accountSubscribeDto.getSponsorshipCode();
    }
}
