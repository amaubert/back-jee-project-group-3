package esgi.project.jee.back.user.controllers;

import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorshipCreateDto;
import esgi.project.jee.back.user.definitions.services.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.security.Principal;

import static esgi.project.jee.back.share.Routes.User.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = Prefix, produces = APPLICATION_JSON_VALUE)
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping(Sponsor)
    public ResponseEntity<SponsorShipDto> sponsor(@RequestBody @Valid SponsorshipCreateDto sponsorshipCreateDto,
                                                  Principal principal) {
        var  sponsorShipDto = userService.createSponsorship(sponsorshipCreateDto, principal);
        return ResponseEntity.ok(sponsorShipDto);
    }
}
