package esgi.project.jee.back.user.definitions.dtos;

import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.user.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String lastName;
    private String firstName;
    private String phone;
    private String email;
    private AccountDto account;

    public UserDto(User user) {
        this.id = user.getId();
        this.account = new AccountDto(user.getAccount());
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.phone = user.getPhone();
    }
}
