package esgi.project.jee.back.user.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class UserNotFoundException extends ResourceNotFoundException {
    public UserNotFoundException(String message) {
        super(message);
    }
}
