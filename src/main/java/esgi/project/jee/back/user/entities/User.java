package esgi.project.jee.back.user.entities;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.user.definitions.dtos.UserCreateDto;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity(name = "user")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String firstName;

    @Column(unique = true)
    private String phone;

    @Column( unique = true, nullable = false)
    private String email;

    @OneToOne( fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false,
                foreignKey = @ForeignKey(name = "fk_user_account",
                value = ConstraintMode.CONSTRAINT))
    private Account account;

    public User(UserCreateDto userCreateDto) {
        this.lastName = userCreateDto.getLastName();
        this.firstName = userCreateDto.getFirstName();
        this.account = new Account(userCreateDto.getAccount());
        this.email = userCreateDto.getEmail();
        this.phone = userCreateDto.getPhone();
    }

    public User(UserDto userDto) {
        this.id = userDto.getId();
        this.lastName = userDto.getLastName();
        this.firstName = userDto.getFirstName();
        this.phone = userDto.getPhone();
        this.email = userDto.getEmail();
        this.account = new Account(userDto.getAccount());
    }
}
