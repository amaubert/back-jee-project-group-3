package esgi.project.jee.back.user.definitions.services;

import esgi.project.jee.back.account.defintions.dtos.AccountUpdateDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorshipCreateDto;
import esgi.project.jee.back.user.definitions.dtos.UserCreateDto;
import esgi.project.jee.back.user.definitions.dtos.UserDto;

import java.security.Principal;

public interface IUserService {
    UserDto create(UserCreateDto userCreateDto);
    UserDto findByAccountId(Long accountId);
    UserDto findById(Long userId);
    UserDto findByAccountLogin(String accountLogin);
    SponsorShipDto createSponsorship(SponsorshipCreateDto sponsorshipCreateDto, Principal principal);
    UserDto update(AccountUpdateDto accountUpdateDto, String accountLogin);
}
