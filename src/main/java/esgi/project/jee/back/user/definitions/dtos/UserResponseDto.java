package esgi.project.jee.back.user.definitions.dtos;

import esgi.project.jee.back.account.defintions.dtos.AccountResponseDto;
import esgi.project.jee.back.user.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private Long id;
    private String lastName;
    private String firstName;
    private String phone;
    private String email;
    private AccountResponseDto account;

    public UserResponseDto(UserDto userDto) {
        this.id = userDto.getId();
        this.lastName = userDto.getLastName();
        this.firstName = userDto.getFirstName();
        this.phone = userDto.getPhone();
        this.email = userDto.getEmail();
        this.account = new AccountResponseDto(userDto.getAccount());

    }

    public UserResponseDto(User user) {
        this.id = user.getId();
        this.lastName = user.getLastName();
        this.firstName = user.getFirstName();
        this.phone = user.getPhone();
        this.email = user.getEmail();
        this.account = new AccountResponseDto(user.getAccount());
    }
}
