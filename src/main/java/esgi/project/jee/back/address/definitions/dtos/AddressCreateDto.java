package esgi.project.jee.back.address.definitions.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AddressCreateDto {
    @NotNull(message = "{property-number.null}")
    private String propertyNumber;
    @NotNull(message = "{street-name.null}")
    private String streetName;
    @NotNull(message = "{postal-code.null}")
    private Integer postalCode;
    @NotNull(message = "{city.null}")
    private String city;
    @NotNull(message = "{country.null}")
    private String country;

    public AddressCreateDto(@NotNull String propertyNumber,
                            @NotNull String streetName,
                            @NotNull Integer postalCode,
                            @NotNull String city,
                            @NotNull String country){
        this.propertyNumber = propertyNumber;
        this.streetName = streetName;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }

    public AddressCreateDto(AddressDto address) {
        this.propertyNumber = address.getPropertyNumber();
        this.streetName = address.getStreetName();
        this.postalCode = address.getPostalCode();
        this.city = address.getCity();
        this.country = address.getCountry();
    }
}
