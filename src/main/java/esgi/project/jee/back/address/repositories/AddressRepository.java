package esgi.project.jee.back.address.repositories;

import esgi.project.jee.back.address.entities.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long>, QueryByExampleExecutor<Address> {
    Optional<Address> findByPropertyNumberAndAndStreetNameAndPostalCodeAndCityAndCountry(String propertyNumber, String streetName, int postalCode, String city, String country);
}