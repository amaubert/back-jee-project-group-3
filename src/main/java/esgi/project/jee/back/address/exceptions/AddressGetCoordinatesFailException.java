package esgi.project.jee.back.address.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class AddressGetCoordinatesFailException extends ResourceNotFoundException {
    public AddressGetCoordinatesFailException(String message) {
        super(message);
    }
}
