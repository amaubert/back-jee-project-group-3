package esgi.project.jee.back.address.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceCreateFailedException;

public class AddressCreateFailedException extends ResourceCreateFailedException {
    public AddressCreateFailedException(String message) {
        super(message);
    }
}
