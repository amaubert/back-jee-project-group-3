package esgi.project.jee.back.address.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceCreationFailedException;

public class AddressCreationFailedException  extends ResourceCreationFailedException {
    public AddressCreationFailedException(String message) {
        super(message);
    }
}
