package esgi.project.jee.back.address.entities;

import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity(name = "Address")
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

    private String propertyNumber;
    private String streetName;
    private Integer postalCode;
    private String city;
    @Column(columnDefinition = "varchar(255) default 'france'")
    private String country;
    @Column(columnDefinition = "Decimal(10,6)")
    private Double longitude;
    @Column(columnDefinition = "Decimal(10,6)")
    private Double latitude;

    public Address(){
    }

    public Address(AddressCreateDto addressCreateDto){
        this.propertyNumber = addressCreateDto.getPropertyNumber();
        this.streetName = addressCreateDto.getStreetName();
        this.postalCode = addressCreateDto.getPostalCode();
        this.city = addressCreateDto.getCity();
        this.country = addressCreateDto.getCountry();
    }

    public Address(AddressDto addressDto) {
        this.id = addressDto.getId();
        this.propertyNumber = addressDto.getPropertyNumber();
        this.streetName = addressDto.getStreetName();
        this.postalCode = addressDto.getPostalCode();
        this.country = addressDto.getCountry();
        this.city = addressDto.getCity();
        this.longitude = addressDto.getLongitude();
        this.latitude = addressDto.getLatitude();
    }

}



