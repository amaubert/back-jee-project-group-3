package esgi.project.jee.back.address.services;

import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.address.repositories.AddressRepository;
import esgi.project.jee.back.address.entities.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;


import java.io.*;
import java.util.Optional;

@Component
public class AddressService implements IAddressService {

    private final AddressRepository addressRepository ;
    private final IAPIAddress iapiAddress;


    @Autowired
    public AddressService(AddressRepository addressRepository, IAPIAddress iapiAddress) {
        this.addressRepository = addressRepository;
        this.iapiAddress = iapiAddress;
    }

    @Override
    public Optional<Address> fetchAddress(String propertyNumber, String streetName,
                                          int postCode, String city, String country) {
        //TODO create an Enum or A constant for this value
        String tmp_country = country == null ? "france" : country;
        return addressRepository.
                findByPropertyNumberAndAndStreetNameAndPostalCodeAndCityAndCountry(propertyNumber, streetName,
                                                                                    postCode, city, tmp_country);
    }

    @Override
    public Optional<AddressDto> create(Address address) {
        try{

            var optionalFetchedAddress = this.fetchAddress( address.getPropertyNumber(), address.getStreetName(),
                                                            address.getPostalCode(), address.getCity(),
                                                            address.getCountry());

            if (optionalFetchedAddress.isPresent()){
                return Optional.empty();
            }

            var response = iapiAddress.getCoordinates(iapiAddress.stringifyAddress(address), address.getPostalCode());
            var addressAPIDto = response.getBody();

            //TODO Create a custom Exception for this
            if ( addressAPIDto == null ||
                //TODO create an Enum or A constant for the Point Value
                !addressAPIDto.getFeatures()[0].getGeometry().getType().equals("Point") ){
                throw new IOException();
            }

            //TODO refacto this if possible
            var firstFeature = addressAPIDto.getFeatures()[0];
            address.setLongitude(firstFeature.getGeometry().getCoordinates()[0]);
            address.setLatitude(firstFeature.getGeometry().getCoordinates()[1]);

            if (address.getCountry() == null){
                address.setCountry("france"); //TODO create an Enum or A constant for this value
            }

            var addressSaved = addressRepository.save(address);
            var addressSavedDto = new AddressDto(addressSaved);

            return Optional.of(addressSavedDto);
        } catch (DataIntegrityViolationException | IOException e) {
            return Optional.empty();
        }
    }


}
