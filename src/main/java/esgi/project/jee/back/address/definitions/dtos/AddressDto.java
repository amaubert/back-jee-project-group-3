package esgi.project.jee.back.address.definitions.dtos;

import esgi.project.jee.back.address.entities.Address;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

    @NotNull
    private Long id;
    @NotNull
    private String propertyNumber;
    @NotNull
    private String streetName;
    @NotNull
    private Integer postalCode;
    @NotNull
    private String city;
    @NotNull
    private String country;
    @NotNull
    private Double longitude;
    @NotNull
    private Double latitude;

    public AddressDto( @NotNull Address address) {
        this.id = address.getId();
        this.propertyNumber = address.getPropertyNumber();
        this.streetName = address.getStreetName();
        this.postalCode = address.getPostalCode();
        this.city = address.getCity();
        this.country = address.getCountry();
        this.longitude = address.getLongitude();
        this.latitude = address.getLatitude();

    }
}
