package esgi.project.jee.back.address.definitions;

import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.address.entities.Address;

import java.util.Optional;

public interface IAddressService {
    Optional<Address> fetchAddress(String propertyNumber, String streetName,  int postCode, String city, String country);
    Optional<AddressDto> create(Address address);
}
