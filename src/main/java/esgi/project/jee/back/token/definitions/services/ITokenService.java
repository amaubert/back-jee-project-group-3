package esgi.project.jee.back.token.definitions.services;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.token.entities.Token;
import esgi.project.jee.back.security.definitions.ITokenProvider;
import org.springframework.security.core.Authentication;

public interface ITokenService {
    Token create(Authentication authentication, Account account);
    boolean validateToken(String jwtToken);
    ITokenProvider getTokenProvider();
    void delete(String jwtToken);
}
