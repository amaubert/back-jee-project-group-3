package esgi.project.jee.back.token.exceptions;

import esgi.project.jee.back.share.exceptions.ResourceNotFoundException;

public class TokenNotFoundException extends ResourceNotFoundException {
    public TokenNotFoundException(String message) {
        super(message);
    }
}
