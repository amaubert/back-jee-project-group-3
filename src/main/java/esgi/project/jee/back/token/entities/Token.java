package esgi.project.jee.back.token.entities;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Token extends BaseEntity {

    @NotNull
    @NotEmpty
    private String token;

    @ManyToOne( fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false,
                foreignKey = @ForeignKey(name = "fk_token_accounts",
                                    value = ConstraintMode.CONSTRAINT))
    private Account account;

    public Token(String jwtToken, Account account) {
        this.token = jwtToken;
        this.account = account;
    }
}
