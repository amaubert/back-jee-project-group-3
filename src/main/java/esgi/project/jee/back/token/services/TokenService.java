package esgi.project.jee.back.token.services;

import esgi.project.jee.back.token.definitions.services.ITokenService;
import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.token.entities.Token;
import esgi.project.jee.back.token.exceptions.TokenNotFoundException;
import esgi.project.jee.back.token.repositories.TokenRepository;
import esgi.project.jee.back.security.definitions.ITokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TokenService implements ITokenService {

    private final TokenRepository tokenRepository;
    private final ITokenProvider tokenProvider;

    @Autowired
    public TokenService(TokenRepository tokenRepository,
                        ITokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
        this.tokenRepository = tokenRepository;
    }


    @Override
    public Token create(Authentication authentication, Account account) {
        //create the token
        String jwtToken = tokenProvider.createToken(authentication);
        var token = new Token(jwtToken, account);
        return tokenRepository.save(token);
    }

    @Override
    public boolean validateToken(String jwtToken) {
        var  optionalToken = tokenRepository.findByToken(jwtToken);
        if (optionalToken.isEmpty()) {
            return false;
        }

        if ( !tokenProvider.validateToken(jwtToken) ) {
            tokenRepository.delete(optionalToken.get());
            return false;
        }
        return true;
    }

    @Override
    public ITokenProvider getTokenProvider() {
        return tokenProvider;
    }

    @Override
    public void delete(String jwtToken) {
        var token = tokenRepository.findByToken(jwtToken)
                            .orElseThrow(() -> new TokenNotFoundException("Token ("+ jwtToken + ") not found ."));
        tokenRepository.delete(token);
    }


}
