package esgi.project.jee.back.utils;

import java.util.Random;

public class OtpCodeUtil {
    private final static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final static Random random = new Random();

    public static String generateCode() {
        StringBuilder stringBuilder = new StringBuilder();
        var randomNumber = Math.abs(random.nextInt()) % alphabet.length();
        stringBuilder.append(alphabet.charAt(randomNumber));
        for( int i = 0 ; i < 5; i++) {
            randomNumber = Math.abs(random.nextInt()) % alphabet.length();
            stringBuilder.append(alphabet.charAt(randomNumber));
        }
        return stringBuilder.toString();
    }
}
