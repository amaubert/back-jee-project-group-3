package esgi.project.jee.back.utils;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil {
    public static final String TOKEN_PREFIX = "Bearer ";

    private String secretKey;

    private Long validityTokenInMilliseconds;

    public JwtTokenUtil(@Value("${security.jwt.token.secret-key:secret}") String secretKey,
                        @Value("${jwt.token.expire-length:1800000 }") Long validityTokenInMilliseconds) { //1 800 000 ms == 30m
        this.secretKey = secretKey;
        this.validityTokenInMilliseconds = validityTokenInMilliseconds;
    }

    @PostConstruct
    public void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String generateToken(String subject, Map<String, Object> claims) {
        //TODO create DateProvider ?
        Date now = new Date();
        Date validityDate = new Date(now.getTime() + validityTokenInMilliseconds);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(now)
                .setExpiration(validityDate)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    //retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }
    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        try{
            final Date expiration = getExpirationDateFromToken(token);
            return expiration.before(new Date());
        } catch (ExpiredJwtException e){
            return true;
        }
    }

    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieving any information from token we will need the secret key
    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    public boolean isValid(String token) {
        try{
            getAllClaimsFromToken(token);
            return !isTokenExpired(token);
        }catch (SignatureException | ExpiredJwtException e) {
            return false;
        }

    }
}
