package esgi.project.jee.back.utils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;

public class CustomHttpServletRequestWrapper extends HttpServletRequestWrapper {
    private String body;

    public CustomHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        BufferedInputStream bufferedInputStream = new BufferedInputStream( request.getInputStream());
        body = bufferedInputStreamToString(bufferedInputStream);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        var bytes = body.getBytes();
        return new ServletInputStream() {
            private int lastIndexRetrieved = -1;
            private ReadListener readListener = null;

            @Override
            public boolean isFinished() {
                return (lastIndexRetrieved == bytes.length - 1);
            }

            @Override
            public boolean isReady() {
                // This implementation will never block
                // We also never need to call the readListener from this method, as this method will never return false
                return isFinished();
            }

            @Override
            public void setReadListener(ReadListener readListener) {
                this.readListener = readListener;
                if (!isFinished()) {
                    try {
                        readListener.onDataAvailable();
                    } catch (IOException e) {
                        readListener.onError(e);
                    }
                } else {
                    try {
                        readListener.onAllDataRead();
                    } catch (IOException e) {
                        readListener.onError(e);
                    }
                }
            }

            @Override
            public int read() throws IOException {
                int i;
                if (!isFinished()) {
                    i = bytes[lastIndexRetrieved + 1];
                    lastIndexRetrieved++;
                    if (isFinished() && (readListener != null)) {
                        try {
                            readListener.onAllDataRead();
                        } catch (IOException ex) {
                            readListener.onError(ex);
                            throw ex;
                        }
                    }
                    return i;
                } else {
                    return -1;
                }
            }
        };
    }

    private String bufferedInputStreamToString(BufferedInputStream bufferedInputStream) throws IOException {
        byte[] contents = new byte[1024];

        int bytesRead = 0;
        StringBuilder strFileContents = new StringBuilder();
        while((bytesRead = bufferedInputStream.read(contents)) != -1) {
            strFileContents.append(new String(contents, 0, bytesRead));
        }
        return strFileContents.toString();
    }

    public String getBody() {
        return body;
    }

    @Override
    public BufferedReader getReader() throws IOException {

        InputStream inputStream = new ByteArrayInputStream(body.getBytes());
        InputStreamReader isr = new InputStreamReader(inputStream);
        return new BufferedReader(isr);
    }

    public void setBody(String body) {
        this.body = body;
    }
}

