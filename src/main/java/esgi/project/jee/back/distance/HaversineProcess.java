package esgi.project.jee.back.distance;

import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.GeometryAPIDto;

public class HaversineProcess {

    public static Double process(AddressDto addressDto, GeometryAPIDto geometryAPIDto){
        final int R = 6371;
        var coord = geometryAPIDto.getCoordinates();
        Double addressDtoLatitude = addressDto.getLatitude();
        Double addressDtoLongitude = addressDto.getLongitude();
        Double newAddressLatitude = coord[1];
        Double newAddressLongitude = coord[0];

        double latDistance = toRad(newAddressLatitude-addressDtoLatitude);
        double lonDistance = toRad(newAddressLongitude-addressDtoLongitude);

        //d = 6371 * c
        //c = 2 arctan(√a / √1-a)
        //a = sin²(φ2 - φ1) + cos(φ1) * cos(φ2) * sin²(λ2 - λ1)
        // => 6371 : rayon de la terre
        // => λ : longitude (km)
        // => φ : latitude (rad)

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(addressDtoLatitude)) * Math.cos(toRad(newAddressLatitude)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return Math.floor(R * c * 100) / 100;
    }

    public static Double toRad(Double val){
        return val * Math.PI / 180;
    }
}
