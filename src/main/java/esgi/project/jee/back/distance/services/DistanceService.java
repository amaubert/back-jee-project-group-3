package esgi.project.jee.back.distance.services;

import esgi.project.jee.back.distance.definitions.dtos.DistanceDto;
import esgi.project.jee.back.distance.HaversineProcess;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.distance.definitions.IDistanceService;
import esgi.project.jee.back.property.definition.IPropertyService;
import esgi.project.jee.back.address.exceptions.AddressGetCoordinatesFailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DistanceService implements IDistanceService {
    private final IPropertyService propertyService;
    private final IAPIAddress apiAddress;

    @Autowired
    public DistanceService(IPropertyService propertyService,
                           IAPIAddress apiAddress) {
        this.apiAddress = apiAddress;
        this.propertyService = propertyService;
    }

    @Override
    public DistanceDto distanceFromProperty(Long propertyId, String address) {
        var propertyAddress = propertyService.findPropertyById(propertyId).getAddress();
        try {
            var coordinates = apiAddress.getCoordinates(address);
            var distance=  HaversineProcess.process(propertyAddress, coordinates);
            return new DistanceDto(distance, "km");
        } catch (IOException e) {
            throw new AddressGetCoordinatesFailException("Cannot get the coordinates from the external API");
        }
    }
}
