package esgi.project.jee.back.distance.controllers;

import esgi.project.jee.back.distance.definitions.dtos.DistanceDto;
import esgi.project.jee.back.distance.definitions.IDistanceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static esgi.project.jee.back.share.Routes.Distance.Distance2Points;
import static esgi.project.jee.back.share.Routes.Distance.Prefix;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping( path = Prefix,produces = APPLICATION_JSON_VALUE)
public class DistanceController {

    private final IDistanceService distanceService;

    public DistanceController( IDistanceService distanceService) {
        this.distanceService = distanceService;
    }

    @GetMapping(Distance2Points)
    public ResponseEntity<DistanceDto> getDistance(@PathVariable("propertyId") Long id,
                                                   @RequestParam("address") String address){
        var distanceDto = distanceService.distanceFromProperty(id, address);
        return ResponseEntity.ok(distanceDto);
    }
}
