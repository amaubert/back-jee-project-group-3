package esgi.project.jee.back.distance.definitions;

import esgi.project.jee.back.distance.definitions.dtos.DistanceDto;

public interface IDistanceService {
    DistanceDto distanceFromProperty(Long propertyId, String address);
}
