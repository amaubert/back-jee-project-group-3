package esgi.project.jee.back.distance.definitions.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistanceDto {
    private Double distance; // Distance en Km
    private String unity;
}
