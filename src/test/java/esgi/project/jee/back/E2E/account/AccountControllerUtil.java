package esgi.project.jee.back.E2E.account;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import io.restassured.RestAssured;

import static esgi.project.jee.back.share.Routes.Account.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class AccountControllerUtil {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public UserResponseDto subscribeAndActiveAccount(AccountSubscribeDto accountSubscribeDto)
                                                        throws JsonProcessingException {
        var uriSubscribe = completeUri(Subscribe);
        var bodySubscribe = objectMapper.writeValueAsString(accountSubscribeDto);
        var codeToValidateDto = RestAssured.given()
                                            .when()
                                                .body(bodySubscribe)
                                                .contentType(APPLICATION_JSON_VALUE)
                                                .post(uriSubscribe)
                                            .then()
                                                .statusCode(CREATED.value())
                                                .extract()
                                                .body()
                                                .jsonPath().getObject(".", CodeToValidateDto.class);
        var code = SMSSenderServiceMock.lastCodeGenerated;
        var body = new AccountActivateDto(code);
        RestAssured.given()
                    .when()
                        .body(body)
                        .contentType(APPLICATION_JSON_VALUE)
                        .put(codeToValidateDto.getValidationUrl())
                    .then()
                        .statusCode(NO_CONTENT.value());
        codeToValidateDto.getUser().getAccount().setActivated(true);
        return codeToValidateDto.getUser();
    }

    public AccountLoggedDto connectAccount(String login, String password) throws JsonProcessingException {
        var uri = completeUri(Login);
        var accountLoginDto = new AccountLoginDto(login, password);
        var body = objectMapper.writeValueAsString(accountLoginDto);

        return RestAssured.given()
                                            .when()
                                                .body(body)
                                                .contentType(APPLICATION_JSON_VALUE)
                                                .post(uri)
                                            .then()
                                                .statusCode(OK.value())
                                                .extract()
                                                .body()
                                                .jsonPath().getObject(".", AccountLoggedDto.class);
    }

}
