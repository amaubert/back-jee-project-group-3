package esgi.project.jee.back.E2E;

import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("e2eTest")
@Component
public class ConfigE2E {
    @Bean
    @Primary
    public ISMSSender ismsSenderMock() {
        return new SMSSenderServiceMock();
    }
}
