package esgi.project.jee.back.E2E.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import esgi.project.jee.back.E2E.account.AccountControllerUtil;
import esgi.project.jee.back.account.defintions.dtos.AccountSubscribeDto;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorshipCreateDto;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static esgi.project.jee.back.share.Routes.User.*;
import static esgi.project.jee.back.security.TokenProvider.Token_Prefix;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@ActiveProfiles("e2eTest")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
public class UserControllerE2ETest {

    @LocalServerPort
    private int port;

    private AccountControllerUtil accountControllerUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private AccountSubscribeDto defaultAdminAccount ;

    @BeforeEach
    void setup() {
        RestAssured.port = port;

        accountControllerUtil = new AccountControllerUtil();

        defaultAdminAccount = new AccountSubscribeDto("admin","adminadmin",
                "lastname","firstname",
                "33600000000","admin@myemail.com",true);
    }

    @Test
    void should_create_sponsorship_successfully() throws JsonProcessingException {
        accountControllerUtil.subscribeAndActiveAccount(defaultAdminAccount);
        var accountLoggedDto = accountControllerUtil.connectAccount(defaultAdminAccount.getLogin(),
                                                                     defaultAdminAccount.getPassword());
        var phoneNumber = "33600990099";
        var sponsorshipCreateDto = new SponsorshipCreateDto(phoneNumber);
        var body = objectMapper.writeValueAsString(sponsorshipCreateDto);
        var uri = completeUri(Sponsor);
        var headerAuthorization = new Header(AUTHORIZATION, Token_Prefix + accountLoggedDto.getToken());
        var sponsorShipDto = RestAssured.given()
                                            .log().all()
                                        .when()
                                            .header(headerAuthorization)
                                            .body(body)
                                            .contentType(APPLICATION_JSON_VALUE)
                                            .post(uri)
                                        .then()
                                            .statusCode(OK.value())
                                            .extract()
                                            .body()
                                            .jsonPath().getObject(".", SponsorShipDto.class);

        assertThat(sponsorShipDto).extracting(SponsorShipDto::getCode)
                                    .isEqualTo(SMSSenderServiceMock.lastCodeGenerated);
    }

    @Test
    void should_sponsor_a_new_create_user() throws JsonProcessingException {
        accountControllerUtil.subscribeAndActiveAccount(defaultAdminAccount);
        var accountLoggedDto = accountControllerUtil.connectAccount(defaultAdminAccount.getLogin(),
                defaultAdminAccount.getPassword());
        var phoneNumber = "33600990099";
        var sponsorshipCreateDto = new SponsorshipCreateDto(phoneNumber);
        var bodySponsor = objectMapper.writeValueAsString(sponsorshipCreateDto);
        var uriSponsor = completeUri(Sponsor);
        var headerAuthorization = new Header(AUTHORIZATION, Token_Prefix + accountLoggedDto.getToken());
        var sponsorShipDto = RestAssured.given()
                                        .when()
                                            .header(headerAuthorization)
                                            .body(bodySponsor)
                                            .contentType(APPLICATION_JSON_VALUE)
                                            .post(uriSponsor)
                                        .then()
                                            .statusCode(OK.value())
                                            .extract()
                                            .body()
                                            .jsonPath().getObject(".", SponsorShipDto.class);

        var newUser = new AccountSubscribeDto("user","useruser",
                "user","user",
                phoneNumber,"user@myemail.com",false, sponsorShipDto.getCode());
        // status code checked in method subscribeAndActiveAccount
        accountControllerUtil.subscribeAndActiveAccount(newUser);
    }

}
