package esgi.project.jee.back.E2E.account;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static esgi.project.jee.back.share.Routes.Account.*;
import static esgi.project.jee.back.security.TokenProvider.Token_Prefix;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@ActiveProfiles("e2eTest")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
public class AccountControllerTest {

    @LocalServerPort
    private int port;

    private AccountControllerUtil accountControllerUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private AccountSubscribeDto defaultAdminAccount ;

    @BeforeEach
    void setup() {
        RestAssured.port = port;

        accountControllerUtil = new AccountControllerUtil();

        defaultAdminAccount = new AccountSubscribeDto("admin","adminadmin",
                                                    "lastname","firstname",
                                                    "33600000000","admin@myemail.com",true);

    }

    @Test
    void Should_subscribe_an_account_successfully() throws JsonProcessingException {
        var uri = completeUri(Subscribe);
        var accountSubscribeDto = new AccountSubscribeDto("admin","adminadmin",
                                                        "lastname","firstname",
                                                                "33600000000","admin@myemail.com",true);
        var body = objectMapper.writeValueAsString(accountSubscribeDto);
        var codeToValidateDto = RestAssured.given()
                                        .log().all()
                                    .when()
                                        .body(body)
                                        .contentType(APPLICATION_JSON_VALUE)
                                        .post(uri)
                                    .then()
                                        .statusCode(CREATED.value())
                                        .extract()
                                        .body()
                                        .jsonPath().getObject(".", CodeToValidateDto.class);

        assertThat(codeToValidateDto).extracting(CodeToValidateDto::getValidationUrl)
                                        .isNotNull();
        assertThat(codeToValidateDto).extracting(CodeToValidateDto::getUser)
                                        .isNotNull();
        var user = codeToValidateDto.getUser();
        assertThat(user).extracting(UserResponseDto::getFirstName)
                        .isEqualTo(defaultAdminAccount.getFirstName());
        assertThat(user).extracting(UserResponseDto::getLastName)
                        .isEqualTo(defaultAdminAccount.getLastName());
        assertThat(user).extracting(UserResponseDto::getEmail)
                        .isEqualTo(defaultAdminAccount.getEmail());
        assertThat(user).extracting(UserResponseDto::getPhone)
                        .isEqualTo(defaultAdminAccount.getPhone());
        assertThat(user).extracting(UserResponseDto::getFirstName)
                        .isEqualTo(defaultAdminAccount.getFirstName());

    }

    @Test
    void should_active_account() throws JsonProcessingException {
        var uriSubscribe = completeUri(Subscribe);
        var bodySubscribe = objectMapper.writeValueAsString(defaultAdminAccount);
        var codeToValidateDto = RestAssured.given()
                                                .log().all()
                                            .when()
                                                .body(bodySubscribe)
                                                .contentType(APPLICATION_JSON_VALUE)
                                                .post(uriSubscribe)
                                            .then()
                                                .statusCode(CREATED.value())
                                                .extract()
                                                .body()
                                                .jsonPath().getObject(".", CodeToValidateDto.class);
        var code = SMSSenderServiceMock.lastCodeGenerated;
        var body = new AccountActivateDto(code);
        RestAssured.given()
                        .log().all()
                    .when()
                        .body(body)
                        .contentType(APPLICATION_JSON_VALUE)
                        .put(codeToValidateDto.getValidationUrl())
                    .then()
                        .statusCode(NO_CONTENT.value());

    }

    @Test
    void should_connect_account_successfully() throws JsonProcessingException {
        var userResponseDto = accountControllerUtil.subscribeAndActiveAccount(defaultAdminAccount);
        var uri = completeUri(Login);
        var accountLoginDto = new AccountLoginDto(defaultAdminAccount.getLogin(), defaultAdminAccount.getPassword());
        var body = objectMapper.writeValueAsString(accountLoginDto);

        var accountLoggedDto = RestAssured.given()
                                                .log().all()
                                            .when()
                                                .body(body)
                                                .contentType(APPLICATION_JSON_VALUE)
                                                .post(uri)
                                            .then()
                                                .statusCode(OK.value())
                                                .extract()
                                                .body()
                                                .jsonPath().getObject(".",AccountLoggedDto.class);
        assertThat(accountLoggedDto).extracting(AccountLoggedDto::getToken)
                                        .isNotNull();
    }

    @Test
    void should_the_data_from_a_connected_user() throws JsonProcessingException {
        var accountCreated = accountControllerUtil.subscribeAndActiveAccount(defaultAdminAccount);
        var accountLoggedDto = accountControllerUtil.connectAccount(defaultAdminAccount.getLogin(),
                                                                    defaultAdminAccount.getPassword());
        var headerAuthorization = new Header(AUTHORIZATION, Token_Prefix + accountLoggedDto.getToken());
        var uri = completeUri(Me);
        var userResponseDto = RestAssured.given()
                                            .log().all()
                                        .when()
                                            .header(headerAuthorization)
                                            .get(uri)
                                        .then()
                                            .statusCode(OK.value())
                                            .extract()
                                            .body()
                                            .jsonPath().getObject(".", UserResponseDto.class);


        assertThat(userResponseDto).isEqualTo(accountCreated);

    }

    @Test
    void should_disconnect_an_authenticated_user() throws JsonProcessingException {
        var accountCreated = accountControllerUtil.subscribeAndActiveAccount(defaultAdminAccount);
        var accountLoggedDto = accountControllerUtil.connectAccount(defaultAdminAccount.getLogin(),
                                                                    defaultAdminAccount.getPassword());
        var headerAuthorization = new Header(AUTHORIZATION, Token_Prefix + accountLoggedDto.getToken());
        var uri = completeUri(Logout);
        RestAssured.given()
                        .log().all()
                    .when()
                        .header(headerAuthorization)
                        .post(uri)
                    .then()
                        .statusCode(NO_CONTENT.value());

    }
}
