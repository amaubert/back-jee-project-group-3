package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.token.entities.Token;
import esgi.project.jee.back.token.repositories.TokenRepository;

import java.util.Optional;

public class TokenRepositoryMock extends BaseRepositoryMock<Token> implements TokenRepository {

    @Override
    public Optional<Token> findByToken(String jwtToken) {
        return list.stream()
                .filter( eachToken -> jwtToken.equals( eachToken.getToken()) )
                .findFirst();
    }

    @Override
    public Optional<Token> findByAccountId(Long accountId) {
        return list.stream()
                .filter( token -> accountId.equals( token.getAccount().getId()) )
                .findFirst();
    }
}
