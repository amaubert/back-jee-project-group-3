package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.share.plateforms.mysql.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BaseRepositoryMock<T extends BaseEntity> implements CrudRepository<T, Long> {
    protected List<T> list;
    protected Long id;

    public BaseRepositoryMock() {
        list = new ArrayList<>();
        this.id = 1L;
    }


    @Override
    public <S extends T> S save(S s) {
        if( s.getId() != null ) {
            var optional =  list.stream()
                    .filter(element -> element.getId().equals(s.getId()))
                    .findFirst()
                    .map(element -> s);
            if( optional.isEmpty() ) {
                return null;
            }
            return optional.get();
        }
        s.setId(id++);
        list.add(s);
        return s;
    }

    @Override
    public <S extends T> Iterable<S> saveAll(Iterable<S> iterable) {
        iterable.forEach(this::save);
        return iterable;
    }

    @Override
    public Optional<T> findById(Long aLong) {
        return list.stream()
                        .filter(t -> aLong.equals(t.getId()))
                        .findFirst();
    }

    @Override
    public boolean existsById(Long aLong) {
       return findById(aLong).isPresent();
    }

    @Override
    public Iterable<T> findAll() {
        return list;
    }

    @Override
    public Iterable<T> findAllById(Iterable<Long> iterable) {
        return list.stream()
                    .filter(t -> contains(iterable, t.getId()))
                    .collect(Collectors.toList());
    }

    @Override
    public long count() {
        return list.size();
    }

    @Override
    public void deleteById(Long aLong) {
        list.removeIf(t -> aLong.equals(t.getId()));
    }

    @Override
    public void delete(T t) {
        list.remove(t);
    }

    @Override
    public void deleteAll(Iterable<? extends T> iterable) {
        List<T> listToDelete = new ArrayList<>();
        for(T eachElement: iterable) {
            listToDelete.add(eachElement);
        }
        list.removeAll(listToDelete);
    }

    @Override
    public void deleteAll() {
        list = new ArrayList<>();
    }
    private boolean contains(Iterable<Long> iterable, Long aLong) {
        for(Long eachId : iterable) {
            if( eachId.equals(aLong) ) {
                return true;
            }
        }
        return false;
    }
}
