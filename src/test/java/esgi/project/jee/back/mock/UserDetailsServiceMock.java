package esgi.project.jee.back.mock;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.security.definitions.Roles;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

public class UserDetailsServiceMock implements UserDetailsService {

    private final AccountRepository accountRepository;

    public UserDetailsServiceMock(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        var account =  accountRepository.findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("Login Incorrect ."));
        var roles = rolesFromAccount(account);

        return User.builder()
                .username(account.getLogin())
                .password(account.getPassword())
                .roles(roles.toArray(new String[0]))
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }

    private List<String> rolesFromAccount(Account account) {
        var roles = new java.util.ArrayList<>(List.of(Roles.User));
        if ( account.isAdmin()) {
            roles.add(Roles.Admin);
        }

        return roles.stream()
                .map(Enum::name)
                .collect(Collectors.toList());
    }
}
