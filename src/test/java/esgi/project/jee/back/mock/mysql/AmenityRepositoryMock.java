package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.amenity.repositories.AmenityRepository;
import esgi.project.jee.back.amenity.entities.Amenity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AmenityRepositoryMock extends BaseRepositoryMock<Amenity>
                                    implements AmenityRepository {

    @Override
    public Optional<Amenity> findByTypeAndNameAndAddressId(String type, String name, Long addressId) {
        return this.list.stream().filter( amenity -> type.equals(amenity.getType())
                                                    && name.equals(amenity.getName())
                                                    && addressId.equals(amenity.getId()))
                                        .findFirst();

    }

    @Override
    public List<Amenity> findAll() {
        return this.list;
    }

    @Override
    public List<Amenity> findByRange(double latitude, double longitude, double distance,
                                     double factorLatitudeToKm, double factorLongitudeToKm) {
        return list.stream().filter( amenity -> latitude - (distance / factorLatitudeToKm) < amenity.getAddress().getLatitude() &&
                                                latitude + (distance / factorLatitudeToKm) > amenity.getAddress().getLatitude() &&
                                                longitude - (distance / factorLongitudeToKm) < amenity.getAddress().getLongitude() &&
                                                longitude + (distance / factorLongitudeToKm) > amenity.getAddress().getLongitude())
                                            .collect(Collectors.toList());
    }

}
