package esgi.project.jee.back.mock.mysql;


import esgi.project.jee.back.confirmationCode.entities.ConfirmationCode;
import esgi.project.jee.back.confirmationCode.repositories.ConfirmationCodeRepository;

import java.util.Optional;

public class ConfirmationCodeRepositoryMock
                extends BaseRepositoryMock<ConfirmationCode>
                implements ConfirmationCodeRepository {
    @Override
    public Optional<ConfirmationCode> findByCode(String code) {
        return this.list.stream()
                .filter( confirmationCode -> code.equals(confirmationCode.getCode()) )
                .findFirst();
    }

    @Override
    public Optional<ConfirmationCode> findByAccount_Id(Long accountId) {
        return this.list.stream()
                .filter( confirmationCode -> accountId.equals(confirmationCode.getAccount().getId()) )
                .findFirst();
    }
}
