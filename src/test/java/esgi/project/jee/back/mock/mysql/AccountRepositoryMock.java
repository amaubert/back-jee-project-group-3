package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;

import java.util.Optional;

public class AccountRepositoryMock extends BaseRepositoryMock<Account> implements AccountRepository {
    @Override
    public Optional<Account> findByLogin(String login) {
        return list.stream()
                        .filter(account -> login.equals(account.getLogin()))
                        .findFirst();
    }
}
