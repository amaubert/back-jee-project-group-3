package esgi.project.jee.back.mock.gps;

import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.AddressAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.FeaturesAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.GeometryAPIDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.address.entities.Address;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public class AddressAPIMock implements IAPIAddress {

    private Double lat;
    private Double lon;

    public AddressAPIMock(Double lat, Double lon){
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public ResponseEntity<AddressAPIDto> getCoordinates(String address, int postCode) throws IOException {


        Double coord[] = new Double[]{this.lat, this.lon};

        var tmp_geometryApiDto = new GeometryAPIDto("Point", coord);
        var tmp_featuresApiDto = new FeaturesAPIDto(tmp_geometryApiDto);

        FeaturesAPIDto tab[] = new FeaturesAPIDto[]{tmp_featuresApiDto};
        var tmp_addressApiDto = new AddressAPIDto(tab);

        return ResponseEntity.ok(tmp_addressApiDto);
    }

    @Override
    public GeometryAPIDto getCoordinates(String address) throws IOException {
        return null;
    }


    @Override
    public String stringifyAddress(Address address) {
        return null;
    }
}
