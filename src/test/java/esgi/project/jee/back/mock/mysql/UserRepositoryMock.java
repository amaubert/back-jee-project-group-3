package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.user.repositories.UserRepository;

import java.util.List;
import java.util.Optional;

public class UserRepositoryMock extends BaseRepositoryMock<User> implements UserRepository {
    @Override
    public List<User> findAll() {
        return list;
    }

    @Override
    public Optional<User> findByAccountId(Long accountId) {
        return list.stream()
                .filter(user -> accountId.equals(user.getAccount().getId()))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmailOrPhone(String email, String phone) {
        return list.stream()
                .filter(user -> email.equals(user.getEmail()) || phone.equals(user.getPhone()))
                .findFirst();
    }

    @Override
    public Optional<User> findByAccount_Login(String login) {
        return list.stream()
                .filter(user -> login.equals(user.getAccount().getLogin()))
                .findFirst();
    }
}
