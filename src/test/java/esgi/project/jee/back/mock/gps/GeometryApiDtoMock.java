package esgi.project.jee.back.mock.gps;

import esgi.project.jee.back.share.plateforms.gps.definitions.dtos.GeometryAPIDto;

public class GeometryApiDtoMock {

    public GeometryAPIDto mockGeometryAPIDto(Double latitude, Double longitude){
        GeometryAPIDto geometryAPIDto = new GeometryAPIDto();
        geometryAPIDto.setCoordinates(new Double[]{longitude, latitude});
        return geometryAPIDto;
    }
}
