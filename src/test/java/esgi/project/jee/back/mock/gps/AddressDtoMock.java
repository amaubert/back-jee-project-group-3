package esgi.project.jee.back.mock.gps;

import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import lombok.Getter;

@Getter
public class AddressDtoMock {

    public AddressDto mockAddressDto(Double latitude, Double longitude){
        AddressDto addressDto = new AddressDto();
        addressDto.setLatitude(latitude);
        addressDto.setLongitude(longitude);

        return addressDto;
    }
}
