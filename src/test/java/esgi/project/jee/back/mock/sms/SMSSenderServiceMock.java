package esgi.project.jee.back.mock.sms;

import esgi.project.jee.back.share.plateforms.sms.definitions.dtos.ListSMSResponseDto;
import esgi.project.jee.back.share.plateforms.sms.definitions.dtos.SMSResponseDto;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class SMSSenderServiceMock implements ISMSSender {

    public static String lastCodeGenerated ;
    @Override
    public ResponseEntity<ListSMSResponseDto> send(String to, String content) {
        var body = new ListSMSResponseDto();
        List<SMSResponseDto> listSmsResponse = new ArrayList<>();
        var splitContent = content.split(":");
        var code = splitContent[splitContent.length - 1];
        lastCodeGenerated = code.trim();
        listSmsResponse.add(new SMSResponseDto(code.trim(), true,
                to, null, null, null));
        body.setMessages(listSmsResponse);

        return ResponseEntity.ok(body);
    }
}
