package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.property.repositories.PropertyRepository;
import esgi.project.jee.back.property.entities.Property;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PropertyRepositoryMock extends BaseRepositoryMock<Property>
                                    implements PropertyRepository {

    public PropertyRepositoryMock(){
        this.list = new ArrayList<>();
    }

    @Override
    public Optional<Property> findByPriceAndSurfaceAndRoomsAndTransactionTypeAndAddressId(Double price, Double surface, int rooms, TransactionType transactionType, Long addressId) {
        return this.list.stream()
                .filter( property -> price.equals(property.getPrice())
                        && surface.equals(property.getSurface())
                        && rooms == property.getRooms()
                        && transactionType.equals(property.getTransactionType())
                        && addressId.equals(property.getAddress().getId()))
                .findFirst();
    }


    @Override
    public List<Property> findAll() {
        return this.list;
    }

    @Override
    public Iterable<Property> findAllById(Iterable<Long> iterable) {
        List<Property> res = new ArrayList<>();
        iterable.forEach(id -> {
            var propertyFound = this.list.stream()
                    .filter(property -> id.equals(property.getId()))
                    .findFirst();
            propertyFound.ifPresent(res::add);
        });
        return res;
    }

    @Override
    public <S extends Property> Optional<S> findOne(Example<S> example) {
        return this.findById(example.getProbe().getId()).map(property -> example.getProbe());
    }

    @Override
    public <S extends Property> Iterable<S> findAll(Example<S> example) {
        return list.stream()
                .filter(property -> property.equals(example.getProbe()))
                .map(property -> example.getProbe())
                .collect(Collectors.toList());
    }

    @Override
    public <S extends Property> Iterable<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Property> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Property> long count(Example<S> example) {
        return list.size();
    }

    @Override
    public <S extends Property> boolean exists(Example<S> example) {
        return this.existsById(example.getProbe().getId());
    }
}
