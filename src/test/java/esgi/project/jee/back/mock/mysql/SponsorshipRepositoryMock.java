package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.sponsorship.entities.Sponsorship;
import esgi.project.jee.back.sponsorship.repositories.SponsorshipRepository;

import java.util.Optional;

public class SponsorshipRepositoryMock extends BaseRepositoryMock<Sponsorship> implements SponsorshipRepository {
    @Override
    public Optional<Sponsorship> findByCode(String code) {
        return list.stream()
                        .filter(sponsorship -> code.equals(sponsorship.getCode()))
                        .findFirst();
    }

    @Override
    public Optional<Sponsorship> findBySponsored_Id(Long id) {
        return list.stream()
                .filter(sponsorship -> id.equals(sponsorship.getSponsored().getId()))
                .findFirst();
    }
}
