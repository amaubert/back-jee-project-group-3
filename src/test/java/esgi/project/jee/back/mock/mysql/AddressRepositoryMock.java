package esgi.project.jee.back.mock.mysql;

import esgi.project.jee.back.address.repositories.AddressRepository;
import esgi.project.jee.back.address.entities.Address;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Optional;
import java.util.stream.Collectors;

public class AddressRepositoryMock  extends BaseRepositoryMock<Address>
                                    implements AddressRepository {

    @Override
    public Optional<Address> findByPropertyNumberAndAndStreetNameAndPostalCodeAndCityAndCountry(String propertyNumber, String streetName, int postalCode, String city, String country) {
        return this.list.stream()
                .filter( address -> propertyNumber.equals(address.getPropertyNumber())
                        && streetName.equals(address.getStreetName())
                        && postalCode == address.getPostalCode()
                        && city.equals(address.getCity())
                        && country.equals(address.getCountry()))
                .findFirst();
    }

    @Override
    public <S extends Address> Optional<S> findOne(Example<S> example) {
        return list.stream()
                .filter(address -> address.equals(example.getProbe()))
                .map( address -> example.getProbe())
                .findFirst();
    }

    @Override
    public <S extends Address> Iterable<S> findAll(Example<S> example) {
        return list.stream()
                .filter(address -> address.equals(example.getProbe()))
                .map(address -> example.getProbe())
                .collect(Collectors.toList());
    }

    @Override
    public <S extends Address> Iterable<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Address> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Address> long count(Example<S> example) {
        return this.list.size();
    }

    @Override
    public <S extends Address> boolean exists(Example<S> example) {
        return this.existsById(example.getProbe().getId());
    }

}
