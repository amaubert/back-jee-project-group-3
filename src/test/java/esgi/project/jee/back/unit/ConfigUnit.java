package esgi.project.jee.back.unit;

import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.confirmationCode.repositories.ConfirmationCodeRepository;
import esgi.project.jee.back.mock.UserDetailsServiceMock;
import esgi.project.jee.back.mock.mysql.*;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import esgi.project.jee.back.sponsorship.repositories.SponsorshipRepository;
import esgi.project.jee.back.token.repositories.TokenRepository;
import esgi.project.jee.back.user.repositories.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


public class ConfigUnit {
    private final ISMSSender smsSenderServiceMock = new SMSSenderServiceMock();

    private final AccountRepository accountRepositoryMock = new AccountRepositoryMock();;
    private final UserRepository userRepositoryMock = new UserRepositoryMock();
    private final TokenRepository tokenRepositoryMock = new TokenRepositoryMock();
    private final ConfirmationCodeRepository confirmationCodeRepositoryMock = new ConfirmationCodeRepositoryMock();
    private final SponsorshipRepository sponsorshipRepositoryMock = new SponsorshipRepositoryMock();

    private final PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    /**
     * The @Primary annotation is there to make sure this instance is
     *  used instead of a real one for autowiring
     */
    @Bean
    @Primary
    public ISMSSender ismsSenderMock() {
        return smsSenderServiceMock;
    }
    @Bean
    @Primary
    public AccountRepository accountRepository() {
        return accountRepositoryMock;
    }
    @Bean
    @Primary
    public UserRepository userRepository() {
        return userRepositoryMock;
    }
    @Bean
    @Primary
    public TokenRepository tokenRepository() {
        return tokenRepositoryMock;
    }
    @Bean
    @Primary
    public ConfirmationCodeRepository confirmationCodeRepository() {
        return confirmationCodeRepositoryMock;
    }
    @Bean
    @Primary
    public SponsorshipRepository sponsorshipRepository() {
        return sponsorshipRepositoryMock;
    }
    @Bean
    @Primary
    public PasswordEncoder passwordEncoder() {
        return bCryptPasswordEncoder;
    }

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceMock(accountRepositoryMock);
    }

}
