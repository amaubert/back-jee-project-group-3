package esgi.project.jee.back.unit.distance;

import esgi.project.jee.back.distance.HaversineProcess;
import esgi.project.jee.back.mock.gps.AddressDtoMock;
import esgi.project.jee.back.mock.gps.GeometryApiDtoMock;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HaversineProcessTest {

    @Test
    void should_return_true_with_0(){

        var rad = HaversineProcess.toRad(0.0);
        assertThat(rad.equals(0.0)).isTrue();
    }

    @Test
    void should_return_true_with_180(){

        var rad = HaversineProcess.toRad(180.0);
        assertThat(rad.equals(3.141592653589793)).isTrue();
    }

    @Test
    void should_return_false_with_90(){

        var rad = HaversineProcess.toRad(90.0);
        assertThat(rad.equals(3.141592653589793)).isFalse();
    }

    @Test
    void should_return_true_between_those_2_addresses(){
        var addressDto = new AddressDtoMock().mockAddressDto(48.858819, 2.294597);
        var geometryDto = new GeometryApiDtoMock().mockGeometryAPIDto(49.252974,4.033212);
        var distance = HaversineProcess.process(addressDto, geometryDto);
        assertThat(distance.equals(134.05)).isTrue();
    }
}
