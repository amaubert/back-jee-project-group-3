package esgi.project.jee.back.unit.account.services;

import esgi.project.jee.back.unit.ConfigUnit;
import esgi.project.jee.back.account.defintions.dtos.*;
import esgi.project.jee.back.account.defintions.services.IAccountService;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.account.services.AccountService;
import esgi.project.jee.back.confirmationCode.repositories.ConfirmationCodeRepository;
import esgi.project.jee.back.confirmationCode.services.ConfirmationCodeService;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.security.TokenProvider;
import esgi.project.jee.back.share.plateforms.sms.definitions.ISMSSender;
import esgi.project.jee.back.sponsorship.repositories.SponsorshipRepository;
import esgi.project.jee.back.sponsorship.services.SponsorshipService;
import esgi.project.jee.back.token.exceptions.TokenNotFoundException;
import esgi.project.jee.back.token.repositories.TokenRepository;
import esgi.project.jee.back.token.services.TokenService;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.user.repositories.UserRepository;
import esgi.project.jee.back.user.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest(classes = {ConfigUnit.class})
@EnableAutoConfiguration(exclude= HibernateJpaAutoConfiguration.class )
public class AccountServiceTest {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConfirmationCodeRepository confirmationCodeRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private SponsorshipRepository sponsorshipRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManagerBuilder authenticationManager;
    @Autowired
    private ISMSSender smsSender;

    private IAccountService accountService;

    private UserResponseDto userTest;

    private AccountDto accountTest;

    @BeforeEach
    void setup() {

        accountRepository.deleteAll();
        userRepository.deleteAll();
        confirmationCodeRepository.deleteAll();
        tokenRepository.deleteAll();
        sponsorshipRepository.deleteAll();

        var confirmationCodeService = new ConfirmationCodeService(confirmationCodeRepository, smsSender);
        var sponsorshipService = new SponsorshipService(sponsorshipRepository, smsSender);
        var userService = new UserService(userRepository, sponsorshipService);
        var tokenProvider = new TokenProvider("secret",1800000L);
        var tokenService = new TokenService(tokenRepository, tokenProvider);
        accountService = new AccountService(accountRepository, passwordEncoder, confirmationCodeService,
                                            userService, authenticationManager, tokenService);


        var accountSubscribeDto = new AccountSubscribeDto("admin","adminadmin","admin",
                "admin","33600000000",
                "admin@myemail.com",true);

        var codeToValidateDto = accountService.subscribe(accountSubscribeDto);

        userTest = codeToValidateDto.getUser();

        var accountResponseDto = userTest.getAccount();

        var confirmationCode = SMSSenderServiceMock.lastCodeGenerated;
        var accountActivateDto = new AccountActivateDto(confirmationCode);

        accountService.activate(accountResponseDto.getId(), accountActivateDto);
        accountTest = new AccountDto(accountResponseDto.getId(), accountResponseDto.isAdmin(),
                                        accountResponseDto.isActivated(),accountSubscribeDto.getLogin(),
                                        accountSubscribeDto.getPassword());
    }

    @Test
    void should_subscribe_successfully() {
        var accountSubscribeDto = new AccountSubscribeDto("user","useruser","user",
                                                        "user","33600000001",
                                                          "user@myemail.com",false);

        var codeToValidateDto = accountService.subscribe(accountSubscribeDto);

        assertThat(codeToValidateDto).isNotNull()
                                        .extracting(CodeToValidateDto::getUser)
                                        .isNotNull()
                                        .isInstanceOf(UserResponseDto.class)
                                        .extracting(UserResponseDto::getAccount)
                                        .isNotNull()
                                        .isInstanceOf(AccountResponseDto.class);

        var userResponseDto = codeToValidateDto.getUser();
        var accountResponseDto = userResponseDto.getAccount();
        assertThat(accountResponseDto).extracting(AccountResponseDto::isActivated)
                                        .isEqualTo(false);
        assertThat(accountResponseDto).extracting(AccountResponseDto::isAdmin)
                                        .isEqualTo(accountSubscribeDto.getIsAdmin());
        assertThat(accountResponseDto).extracting(AccountResponseDto::getId)
                                        .isEqualTo(accountTest.getId() +  1);
        assertThat(userResponseDto).extracting(UserResponseDto::getFirstName)
                                        .isEqualTo(accountSubscribeDto.getFirstName());
        assertThat(userResponseDto).extracting(UserResponseDto::getLastName)
                                        .isEqualTo(accountSubscribeDto.getLastName());
        assertThat(userResponseDto).extracting(UserResponseDto::getEmail)
                                        .isEqualTo(accountSubscribeDto.getEmail());
        assertThat(userResponseDto).extracting(UserResponseDto::getPhone)
                                        .isEqualTo(accountSubscribeDto.getPhone());
    }

    @Test
    void should_validate_confirmation_code_to_activate_account() {
        var accountSubscribeDto = new AccountSubscribeDto("user","useruser","user",
                                                    "user","33600000001",
                                                    "user@myemail.com",false);

        var codeToValidateDto = accountService.subscribe(accountSubscribeDto);
        var accountResponseDto = codeToValidateDto.getUser().getAccount();

        var confirmationCode = SMSSenderServiceMock.lastCodeGenerated;
        var accountActivateDto = new AccountActivateDto(confirmationCode);

        //Throw an exception and test is in echec if active is not success
        accountService.activate(accountResponseDto.getId(), accountActivateDto);
    }

    @Test
    void should_be_logged_successfully() {
        var accountLoginDto = new AccountLoginDto(accountTest.getLogin(), accountTest.getPassword());
        var accountLoggedDto = accountService.login(accountLoginDto);
        assertThat(accountLoggedDto).isNotNull()
                                    .extracting(AccountLoggedDto::getToken)
                                    .isNotNull()
                                    .isNotEqualTo("");
    }

    @Test
    void should_get_user_data_from_connected_user() {
        var accountLoginDto = new AccountLoginDto(accountTest.getLogin(), accountTest.getPassword());
        var accountLoggedDto = accountService.login(accountLoginDto);
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountLoginDto.getLogin(), accountLoginDto.getPassword());

        var userResponseDto = accountService.me(authenticationToken);

        assertThat(userResponseDto).isNotNull()
                                    .extracting(UserResponseDto::getAccount)
                                    .isNotNull();

        assertThat(userResponseDto).extracting(UserResponseDto::getEmail)
                                    .isEqualTo(userTest.getEmail());
        assertThat(userResponseDto).extracting(UserResponseDto::getPhone)
                                        .isEqualTo(userTest.getPhone());
        assertThat(userResponseDto).extracting(UserResponseDto::getLastName)
                                        .isEqualTo(userTest.getLastName());
        assertThat(userResponseDto).extracting(UserResponseDto::getFirstName)
                .isEqualTo(userTest.getFirstName());

        var accountResponseDto = userResponseDto.getAccount();
        assertThat(accountResponseDto).extracting(AccountResponseDto::getId)
                                        .isEqualTo(accountTest.getId());
        assertThat(accountResponseDto).extracting(AccountResponseDto::isAdmin)
                                        .isEqualTo(accountTest.isAdmin());
    }

    @Test
    void should_disconnect_successfully() {
        var accountLoginDto = new AccountLoginDto(accountTest.getLogin(), accountTest.getPassword());
        var  accountLoggedDto= accountService.login(accountLoginDto);
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountLoginDto.getLogin(), accountLoginDto.getPassword());
        var userResponseDto = accountService.me(authenticationToken);

        assertThat(userResponseDto).isNotNull()
                                        .extracting(UserResponseDto::getAccount)
                                        .isNotNull();

        //an exception will be throw if logout
        accountService.logout(accountLoggedDto.getToken());
    }

    @Test
    void should_throw_an_exception_when_a_user_not_logged_want_to_disconnect() {
        var accountLoginDto = new AccountLoginDto(accountTest.getLogin(), accountTest.getPassword());
        var  accountLoggedDto= accountService.login(accountLoginDto);
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountLoginDto.getLogin(), accountLoginDto.getPassword());
        var userResponseDto = accountService.me(authenticationToken);

        assertThat(userResponseDto).isNotNull()
                .extracting(UserResponseDto::getAccount)
                .isNotNull();

        //First logout
        accountService.logout(accountLoggedDto.getToken());

        //user is alreay deconnnected
        assertThatThrownBy(() -> {
            accountService.logout(accountLoggedDto.getToken());
        }).isInstanceOf(TokenNotFoundException.class)
          .hasMessage("Token ("+ accountLoggedDto.getToken()+") not found .");
    }

}
