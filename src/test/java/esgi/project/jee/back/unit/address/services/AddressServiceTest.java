package esgi.project.jee.back.unit.address.services;

import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.mock.gps.AddressAPIMock;
import esgi.project.jee.back.mock.mysql.AddressRepositoryMock;
import esgi.project.jee.back.address.repositories.AddressRepository;
import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.address.services.AddressService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class AddressServiceTest {

    private AddressService addressService;
    private AddressRepository addressRepository;
    private IAPIAddress addressAPI;

    @BeforeEach
    void setup() {
        addressRepository = new AddressRepositoryMock();
        addressAPI = new AddressAPIMock(2.22222, 3.33333);
        addressService = new AddressService(addressRepository, addressAPI);

        // create an Address
        AddressCreateDto addressCreateDto = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", null);
        Address address = new Address(addressCreateDto);
        addressService.create(address);
    }

    @Test
    void should_have_only_one_address() {
        try {
            System.out.println(addressRepository.findAll());
            assertThat(addressRepository.count() == 1)
                    .isTrue();
        } catch (AssertionError e){
            assertThat(e).hasMessage("The address was not created");
        }
    }

    @Test
    void should_create_an_address_with_country_is_france(){
        try {
            Optional<Address> fetchedAddress = addressService.fetchAddress("242", "rue du faubourg saint antoine", 75012, "paris", "france");
            assertThat(fetchedAddress.isPresent())
                    .isTrue();
        }
        catch (AssertionError e){
            assertThat(e).hasMessage("Cannot fetch the address : \"242 rue du faubourg saint antoine 75012 paris\" in france");
        }
    }

    @Test
    void should_create_an_address_with_country_is_usa(){

        addressAPI = new AddressAPIMock(4.44444, 5.55555);
        addressService = new AddressService(addressRepository, addressAPI);

        // create a new Address
        AddressCreateDto addressCreateDto = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", "usa");
        Address address = new Address(addressCreateDto);
        addressService.create(address);


        try {
            Optional<Address> fetchedAddress = addressService.fetchAddress("242", "rue du faubourg saint antoine", 75012, "paris", "usa");
            assertThat(fetchedAddress.isPresent())
                    .isTrue();
        }
        catch (AssertionError e){
            assertThat(e).hasMessage("Cannot fetch the address : \"242 rue du faubourg saint antoine 75012 paris\" in usa");
        }
    }

    @Test
    void should_not_have_2_addresses_because_they_are_the_same() {
        AddressCreateDto addressCreateDto = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", "france");
        Address ad = new Address(addressCreateDto);
        addressService.create(ad);
        try {
            System.out.println(addressRepository.count());
            assertThat(addressRepository.count() > 1)
                    .isFalse();
        } catch (AssertionError e){
            assertThat(e).hasMessage("The address seems to have been created");
        }
    }

    @Test
    void should_have_2_addresses() {
        addressAPI = new AddressAPIMock(4.44444, 5.55555);
        addressService = new AddressService(addressRepository, addressAPI);

        // create a new Address
        AddressCreateDto addressCreateDto = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", "usa");
        Address address = new Address(addressCreateDto);
        addressService.create(address);

        try {
            assertThat(addressRepository.count() == 2)
                    .isTrue();
        } catch (AssertionError e){
            assertThat(e).hasMessage("The second address seems not have been created");
        }
    }

    @Test
    void should_fetch_an_address_with_correct_lat_and_lon() {
        Optional<Address> fetchedAddress = addressService.fetchAddress("242", "rue du faubourg saint antoine", 75012, "paris", "france");

        if (fetchedAddress.isPresent()) {
            var address = fetchedAddress.get();
            try {
                assertThat(address.getLatitude().equals(3.33333) && address.getLongitude().equals(2.22222))
                        .isTrue();
            }
            catch (AssertionError e){
                assertThat(e).hasMessage("Coordinates for \"242 rue du faubourg saint antoine 75012 paris\" are not lat: " + 2.22222 + " - lon: " + 3.33333);
            }
        }
        else {
            fail("no address were found");
        }
    }

    @Test
    void should_not_find_this_address() {

        Optional<Address> fetchedAddress = addressService.fetchAddress("8", "rue jean françois de la perouse", 91300, "massy", "france");

        try {
            assertThat(fetchedAddress.isEmpty())
                    .isTrue();
        }
        catch (AssertionError e){
            assertThat(e).hasMessage("The address \"8 rue jean françois de la perouse\" have been found");
        }
    }
}
