package esgi.project.jee.back.unit.confirmationcode.services;


import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.account.defintions.services.IAccountService;
import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.confirmationCode.definitions.dtos.ConfirmationCodeDto;
import esgi.project.jee.back.confirmationCode.definitions.services.IConfirmationCodeService;
import esgi.project.jee.back.confirmationCode.exceptions.ConfirmationCodeNotFoundException;
import esgi.project.jee.back.confirmationCode.services.ConfirmationCodeService;
import esgi.project.jee.back.mock.mysql.AccountRepositoryMock;
import esgi.project.jee.back.mock.mysql.ConfirmationCodeRepositoryMock;
import esgi.project.jee.back.mock.mysql.UserRepositoryMock;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.user.repositories.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ConfirmationCodeServiceTest {
    private IConfirmationCodeService confirmationCodeService;
    private AccountRepository accountRepository;
    private UserRepository userRepository;

    private Account accountTest;
    private UserDto userTest;

    @BeforeEach
    void setup() {
        accountRepository = new AccountRepositoryMock();
        userRepository = new UserRepositoryMock();

        //Create a Account for test
        Account account = new Account("admin" , "adminadmin",true, true);
        account = accountRepository.save(account);
        accountTest = account;

        //Create a User for test
        var user = new User("lastname" ,"firstname","33600000000",
                                    "admin@myeamil.com", accountTest);
        user = userRepository.save(user);
        userTest = new UserDto(user);

        var confirmationCodeRepository = new ConfirmationCodeRepositoryMock();
        var SMSSender = new SMSSenderServiceMock();
        confirmationCodeService = new ConfirmationCodeService(confirmationCodeRepository,
                                                                SMSSender);
    }

    @Test
    void should_create_confirmation_code_successfully() {
        var confirmationCodeDto = confirmationCodeService.create(accountTest);

        assertThat(confirmationCodeDto).isNotNull()
                                        .extracting(ConfirmationCodeDto::getCode)
                                        .isNotNull();
        assertThat(confirmationCodeDto.getCode()).hasSize(6);
        assertThat(confirmationCodeDto).extracting(ConfirmationCodeDto::getId)
                                        .isNotNull();
        assertThat(confirmationCodeDto).extracting(ConfirmationCodeDto::getAccount)
                                        .extracting(AccountDto::getId)
                                        .isEqualTo(accountTest.getId());

    }

    @Test
    void should_send_code_successfully() {
        var confirmationCodeDto = confirmationCodeService.create(accountTest);

        confirmationCodeService.sendCode(confirmationCodeDto.getCode(), userTest);
        assertThat(SMSSenderServiceMock.lastCodeGenerated).isEqualTo(confirmationCodeDto.getCode());
    }

    @Test
    void should_confirm_a_code_successfully() {
        var confirmationCodeDto = confirmationCodeService.create(accountTest);
        confirmationCodeService.sendCode(confirmationCodeDto.getCode(), userTest);

        var account = confirmationCodeService.confirm(SMSSenderServiceMock.lastCodeGenerated);

        assertThat(account.getId()).isEqualTo(accountTest.getId());
        assertThat(account.getLogin()).isEqualTo(accountTest.getLogin());
        assertThat(account.isAdmin()).isEqualTo(accountTest.isAdmin());
        assertThat(account.isActivated()).isEqualTo(accountTest.isActivated());
    }

    @Test
    void should_throw_an_exception_when_confirm_an_invalid_code() {
        String invalidCode = "Invalid code";
        assertThatThrownBy(() ->
            confirmationCodeService.confirm(invalidCode)
        ).isInstanceOf(ConfirmationCodeNotFoundException.class)
         .hasMessage("Confirmation with code (" + invalidCode +") not found .");
    }
}
