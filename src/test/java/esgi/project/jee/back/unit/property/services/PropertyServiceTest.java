package esgi.project.jee.back.unit.property.services;


import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.address.repositories.AddressRepository;
import esgi.project.jee.back.amenity.definitions.IAmenityService;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.amenity.repositories.AmenityRepository;
import esgi.project.jee.back.amenity.services.AmenityService;
import esgi.project.jee.back.distance.definitions.IDistanceService;
import esgi.project.jee.back.distance.services.DistanceService;
import esgi.project.jee.back.mock.mysql.*;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.property.definition.enums.TransactionType;
import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.property.definition.dtos.PropertyCreateDto;
import esgi.project.jee.back.property.definition.dtos.PropertyDto;
import esgi.project.jee.back.property.definition.dtos.PropertyUpdateDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.property.definition.IPropertyService;
import esgi.project.jee.back.property.exceptions.PropertyNotFoundException;
import esgi.project.jee.back.mock.gps.AddressAPIMock;
import esgi.project.jee.back.property.repositories.PropertyRepository;
import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.property.entities.Property;
import esgi.project.jee.back.address.services.AddressService;
import esgi.project.jee.back.property.services.PropertyService;
import esgi.project.jee.back.sponsorship.definitions.services.ISponsorshipService;
import esgi.project.jee.back.sponsorship.services.SponsorshipService;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.user.repositories.UserRepository;
import esgi.project.jee.back.user.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.security.Principal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PropertyServiceTest {

    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private PropertyRepository propertyRepository ;

    private AmenityRepository amenityRepository;
    private IAmenityService amenityService;

    private IPropertyService propertyService;

    private ISponsorshipService sponsorshipService;

    private AddressDto addressTest;
    private AddressDto amenityAddressTest;
    private PropertyDto propertyTest;
    private UserDto userTest;


    @BeforeEach
    void setup() {
        // Repositories mock
        userRepository = new UserRepositoryMock();
        accountRepository = new AccountRepositoryMock();
        var addressRepository = new AddressRepositoryMock();
        var sponsorshipRepository = new SponsorshipRepositoryMock();
        propertyRepository = new PropertyRepositoryMock();

        //Services mock
        var addressAPI = new AddressAPIMock(2.22222, 3.33333);
        var smsService =new SMSSenderServiceMock();

        //Services
        var addressService = new AddressService(addressRepository, addressAPI);
        sponsorshipService = new SponsorshipService(sponsorshipRepository, smsService);

        // AmenityService
        amenityRepository = new AmenityRepositoryMock();
        amenityService = new AmenityService(amenityRepository , addressService);

        var userService = new UserService(userRepository, sponsorshipService);
        propertyService = new PropertyService(propertyRepository, addressService, userService, sponsorshipService, amenityService);


        //Create a User for test
        var account = new Account("admin", "adminadmin", true, true);
        account = accountRepository.save(account);
        var user = new User("lastname","firstname","33600000000",
                                    "user@myemail.com",account);
        user = userRepository.save(user);
        userTest = new UserDto(user);

        //create an Address for test
        AddressCreateDto addressCreateDto = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", "france");
        //create a Property for Test
        PropertyCreateDto propertyCreateDto = new PropertyCreateDto(100000.00, TransactionType.LEASING, 55.00, 4,addressCreateDto, false );
        propertyTest = propertyService.create(propertyCreateDto);
        addressTest = propertyTest.getAddress();

        //create an Address for test
        AddressCreateDto tmpAddressCreateDto = new AddressCreateDto("", "Place Louis-Armand", 75571, "Paris", "France");
        //create an Amenity for Test
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto("Gare", "Gare de Lyon", tmpAddressCreateDto);
        AmenityDto amenityTest  = amenityService.create(amenityCreateDto);
        amenityAddressTest = amenityTest.getAddress();
    }

    @Test
    void should_have_only_one_property() {
        try {
            assertThat(propertyRepository.count() == 1)
                    .isTrue();
        } catch (AssertionError e){
            assertThat(e).hasMessage("The property was not created");
        }
    }

    @Test
    void should_have_two_properties_at_the_same_address() {
        //create a new Address from the addressTest == Same Address
        var addressCreateDto = new AddressCreateDto(addressTest);
        //create a new property
        PropertyCreateDto propertyCreateDto = new PropertyCreateDto(100000.00, TransactionType.LEASING, 8888.00, 4,addressCreateDto, false );
        var createdProperty = propertyService.create(propertyCreateDto);

        assertThat(createdProperty).isNotNull()
                                    .isNotEqualTo(propertyTest);

        assertThat(createdProperty.getAddress()).isNotNull()
                                                .isEqualTo(addressTest);
        var addressDto = createdProperty.getAddress();
        assertThat(addressDto).extracting(AddressDto::getId)
                                .isEqualTo(addressTest.getId());
        assertThat(addressDto).extracting(AddressDto::getPostalCode)
                                .isEqualTo(addressTest.getPostalCode());
        assertThat(addressDto).extracting(AddressDto::getCity)
                                .isEqualTo(addressTest.getCity());
        assertThat(addressDto).extracting(AddressDto::getPropertyNumber)
                                .isEqualTo(addressTest.getPropertyNumber());
        assertThat(addressDto).extracting(AddressDto::getStreetName)
                                    .isEqualTo(addressTest.getStreetName());
        assertThat(addressDto).extracting(AddressDto::getCountry)
                                    .isEqualTo(addressTest.getCountry());
        assertThat(addressDto).extracting(AddressDto::getLatitude)
                                .isEqualTo(addressTest.getLatitude());
        assertThat(addressDto).extracting(AddressDto::getLongitude)
                                .isEqualTo(addressTest.getLongitude());
    }


    @Test
    void should_have_two_similar_properties_at_different_addresses() {

        //create a new address
        AddressCreateDto newAddressCreateDto = new AddressCreateDto("1", "rue bouvier", 75011, "paris", "france");
        var newPropertyCreateDto = new PropertyCreateDto(propertyTest);
        newPropertyCreateDto.setAddress(newAddressCreateDto); //Different address
        var newPropertyCreated = propertyService.create(newPropertyCreateDto);

        assertThat(newPropertyCreated).isNotNull()
                                        .isInstanceOf(PropertyDto.class)
                                        .isNotEqualTo(propertyTest) // Not The same address
                                        .extracting(PropertyDto::getAddress)
                                        .isNotEqualTo(propertyTest.getAddress());
        assertThat(newPropertyCreated).extracting(PropertyDto::getId)
                                        .isNotNull()
                                        .isNotEqualTo(propertyTest.getId());
        assertThat(newPropertyCreated).extracting(PropertyDto::getRooms)
                                            .isEqualTo(propertyTest.getRooms());
        assertThat(newPropertyCreated).extracting(PropertyDto::getIsAvailable)
                                        .isEqualTo(propertyTest.getIsAvailable());
        assertThat(newPropertyCreated).extracting(PropertyDto::getSurface)
                                        .isEqualTo(propertyTest.getSurface());
        assertThat(newPropertyCreated).extracting(PropertyDto::getTransactionType)
                                        .isEqualTo(propertyTest.getTransactionType());
        assertThat(newPropertyCreated).extracting(PropertyDto::getPrice)
                                        .isEqualTo(propertyTest.getPrice());
        assertThat(newPropertyCreated).extracting(PropertyDto::getPurchaser).isNull();
    }

    @Test
    void should_get_one_property(){
        var listProperties = propertyService.findAllProperties();

        var sameAddressCreateDtoAsAddressTest = new AddressCreateDto("242", "rue du faubourg saint antoine", 75012, "paris", "france");
        var address = new Address(sameAddressCreateDtoAsAddressTest);
        address.setId(1L);
        address.setLongitude(2.22222);
        address.setLatitude(3.33333);

        var samePropertyCreateDtoAdPropertyTest = new PropertyCreateDto(100000.00, TransactionType.LEASING,55.00,4, sameAddressCreateDtoAsAddressTest,false);
        var property = new Property(samePropertyCreateDtoAdPropertyTest);
        property.setId(1L);
        property.setAddress(address);

        var samePropertyDtoAdPropertyTest = new PropertyDto(property);

        assertThat(listProperties).containsOnly(samePropertyDtoAdPropertyTest);

    }

    @Test
    void should_get_two_properties(){
        //Create a new Property
        var sameAddressCreateDtoThatAddressTest = new AddressCreateDto(addressTest);
        PropertyCreateDto propertyCreateDto = new PropertyCreateDto(99.00, TransactionType.SALE,
                                                                    34.00, 4,
                                                                        sameAddressCreateDtoThatAddressTest,true );
        var newPropertyDto = propertyService.create(propertyCreateDto);

        //Fetch all Properties in the Database
        var listProperties = propertyService.findAllProperties();

        assertThat(listProperties).contains(newPropertyDto, propertyTest);
    }

    @Test
    void should_delete_a_property(){
        this.propertyService.delete(1L);
        assertThatThrownBy(() -> {
            this.propertyService.findPropertyById(1L);
        }).isInstanceOf(PropertyNotFoundException.class)
          .hasMessage("Property with ID (1) not found .");
    }

    @Test
    void should_throw_a_PropertyNotFoundException_because_the_property_does_not_exist(){
        assertThatThrownBy(() -> propertyService.delete(2L))
                                    .isInstanceOf(PropertyNotFoundException.class)
                                    .hasMessage("Property with ID (2) not found .");
    }

    @Test
    void should_update_the_entire_property_with_a_new_address(){
        var newAddressCreateDto = new AddressCreateDto("35", "rue du chevalier de la barre", 57018, "paris", "france");
        var newAddressDto = new AddressDto(new Address(newAddressCreateDto));

        //Check that the new Address is not equal to the former address
        assertThat(newAddressDto).isNotEqualTo(addressTest)
                                    .extracting(AddressDto::getStreetName)
                                    .isNotEqualTo(addressTest.getStreetName());

        var currentPropertyDto = this.propertyService.findPropertyById(1L);
        currentPropertyDto.setAddress(newAddressDto);

        var propertyUpdateDto = new PropertyUpdateDto(currentPropertyDto);

        var newPropertyDto = propertyService.update(1L, propertyUpdateDto);

        assertThat(newPropertyDto).isNotEqualTo(propertyTest)
                                    .extracting(PropertyDto::getAddress)
                                    .isNotEqualTo(addressTest)
                                    .extracting(AddressDto::getStreetName)
                                    .isNotEqualTo(addressTest.getStreetName());

    }

    @Test
    void should_update_the_entire_property(){
        //Given
        var currentProperty = this.propertyService.findPropertyById(1L);

        assertThat(currentProperty).isEqualTo(propertyTest); //Check equality

        currentProperty.setIsAvailable(!currentProperty.getIsAvailable());
        currentProperty.setPrice(999999.0);
        currentProperty.setRooms(14);
        currentProperty.setSurface(2800.45);
        currentProperty.setTransactionType(TransactionType.SALE);
        currentProperty.setPurchaser(new UserResponseDto(userTest));

        //When
        var propertyUpdateDto =  new PropertyUpdateDto(currentProperty);
        var updatedPropertyDto = propertyService.update(currentProperty.getId(), propertyUpdateDto);

        assertThat(updatedPropertyDto).isNotEqualTo(propertyTest)
                                        .extracting(PropertyDto::getPrice)
                                        .isNotEqualTo(propertyTest.getPrice());
        assertThat(updatedPropertyDto).extracting(PropertyDto::getSurface)
                                        .isNotEqualTo(propertyTest.getSurface());
        assertThat(updatedPropertyDto).extracting(PropertyDto::getRooms)
                                        .isNotEqualTo(propertyTest.getRooms());
        assertThat(updatedPropertyDto).extracting(PropertyDto::getIsAvailable)
                                        .isNotEqualTo(propertyTest.getIsAvailable());
        assertThat(updatedPropertyDto).extracting(PropertyDto::getTransactionType)
                                        .isNotEqualTo(propertyTest.getTransactionType());

        //Same Address, With haven't change this
        assertThat(updatedPropertyDto).extracting(PropertyDto::getAddress)
                                        .isEqualTo(propertyTest.getAddress());
    }

    @Test
    void should_be_successful_when_an_authenticated_user_buy_a_property() {
        var accountTest = userTest.getAccount();
        Principal principal = new UsernamePasswordAuthenticationToken(  accountTest.getLogin(),
                                                                        accountTest.getPassword() );
        var propertyDto = propertyService.buy(principal, propertyTest.getId());

        assertThat(propertyDto).isNotNull()
                                .isInstanceOf(PropertyDto.class)
                                .extracting(PropertyDto::getIsAvailable)
                                .isEqualTo(false);

        assertThat(propertyDto).extracting(PropertyDto::getPurchaser)
                                .isNotNull()
                                .isEqualTo(new UserResponseDto(userTest));
    }

    @Test
    void should_get_all_properties_with_discount_when_a_connected_user_is_sponsored() {
        //Create a second User
        var newAccount = new Account("sponsored", "sponsored", false, true);
        newAccount = accountRepository.save(newAccount);
        var newUser = new User("sponsored","sponsored","33699999999",
                                "sponsored@myemail.com",newAccount);
        newUser = userRepository.save(newUser);

        //Create sponsorship
        sponsorshipService.createSponsorshipCode(new User(userTest),newUser.getPhone());
        var sponsorshipCode = SMSSenderServiceMock.lastCodeGenerated;
        sponsorshipService.linkSponsorship(sponsorshipCode, newUser);

        var optionalUser = userRepository.findById(newUser.getId());
        assertThat(optionalUser).isPresent();
        var sponsoredAccount = optionalUser.get().getAccount();

        Principal principal = new UsernamePasswordAuthenticationToken(sponsoredAccount.getLogin(),
                                                                      sponsoredAccount.getPassword());

        var propertyDtoListWithDiscount = propertyService.findAllPropertiesWithDiscount(principal);
        var propertyDtoList = propertyService.findAllProperties();

        assertThat(propertyDtoListWithDiscount).hasSize(propertyDtoList.size());
        PropertyDto propertyDtoWithDiscount;
        PropertyDto propertyDto;
        for (int i = 0; i < propertyDtoListWithDiscount.size(); i++)
        {
            propertyDto = propertyDtoList.get(i);
            propertyDtoWithDiscount = propertyDtoListWithDiscount.get(i);
            assertThat(propertyDtoWithDiscount.getId()).isEqualTo(propertyDto.getId());
            assertThat(propertyDtoWithDiscount.getPrice()).isLessThan(propertyDto.getPrice());
        }
    }

    @Test
    void should_get_a_property_by_id_with_a_discount_when_a_connected_user_is_sponsored() {
        //Create a second User
        var newAccount = new Account("sponsored", "sponsored", false, true);
        newAccount = accountRepository.save(newAccount);
        var newUser = new User("sponsored","sponsored","33699999999",
                "sponsored@myemail.com",newAccount);
        newUser = userRepository.save(newUser);

        //Create sponsorship
        sponsorshipService.createSponsorshipCode(new User(userTest),newUser.getPhone());
        var sponsorshipCode = SMSSenderServiceMock.lastCodeGenerated;
        sponsorshipService.linkSponsorship(sponsorshipCode, newUser);

        var optionalUser = userRepository.findById(newUser.getId());
        assertThat(optionalUser).isPresent();
        var sponsoredAccount = optionalUser.get().getAccount();

        Principal principal = new UsernamePasswordAuthenticationToken(sponsoredAccount.getLogin(),
                sponsoredAccount.getPassword());

        var propertyDto = propertyService.findPropertyByIdWithDiscount(principal, propertyTest.getId());

        assertThat(propertyDto.getPrice()).isLessThan(propertyTest.getPrice());
    }

    @Test
    void should_return_error_if_property_doesnt_exists() {
        assertThatThrownBy(() -> propertyService.getAllAmenitiesInRange(2L, 15.0))
                .isInstanceOf(PropertyNotFoundException.class)
                .hasMessage("Property with ID (2) not found .");
    }

    @Test
    void should_get_one_amenity_in_range() {
        List<AmenityDto> amenities = propertyService.getAllAmenitiesInRange(propertyTest.getId(), 15.0);
        assertThat(amenities.size()).isEqualTo(1);
    }

    @Test
    void should_get_two_amenities_at_the_same_coordinates_in_range() {
        //create an Address for test
        AddressCreateDto tmpAddressCreateDto = new AddressCreateDto("205", "Rue Bonaparte", 91100, "Evry", "France");
        //create an Amenity for Test
        AmenityCreateDto tmpAmenityCreateDto = new AmenityCreateDto("Ecole publique", "Alexandre Dumas", tmpAddressCreateDto);
        amenityService.create(tmpAmenityCreateDto);

        List<AmenityDto> amenities = propertyService.getAllAmenitiesInRange(propertyTest.getId(), 15.0);
        assertThat(amenities.size()).isEqualTo(2);
    }

    @Test
    void should_get_two_amenities_at_different_coordinates_in_range() {
        var addressRepository = new AddressRepositoryMock();
        var amenityAddressAPI = new AddressAPIMock(2.22223, 3.33332);
        var amenityAddressService = new AddressService(addressRepository, amenityAddressAPI);
        amenityService = new AmenityService(amenityRepository , amenityAddressService);

        //create an Address for test
        AddressCreateDto tmpAddressCreateDto = new AddressCreateDto("205", "Rue Bonaparte", 91100, "Evry", "France");
        //create an Amenity for Test
        AmenityCreateDto tmpAmenityCreateDto = new AmenityCreateDto("Ecole publique", "Alexandre Dumas", tmpAddressCreateDto);
        amenityService.create(tmpAmenityCreateDto);

        List<AmenityDto> amenities = propertyService.getAllAmenitiesInRange(propertyTest.getId(), 15.0);
        assertThat(amenities.size()).isEqualTo(2);
    }


    @Test
    void should_get_one_amenity_in_range_but_not_the_one_out_of_range() {
        var addressRepository = new AddressRepositoryMock();
        var amenityAddressAPI = new AddressAPIMock(48.849518, 2.388322);
        var amenityAddressService = new AddressService(addressRepository, amenityAddressAPI);
        amenityService = new AmenityService(amenityRepository , amenityAddressService);

        //create an Address for test
        AddressCreateDto tmpAddressCreateDto = new AddressCreateDto("205", "Rue Bonaparte", 91100, "Evry", "France");
        //create an Amenity for Test
        AmenityCreateDto tmpAmenityCreateDto = new AmenityCreateDto("Ecole publique", "Alexandre Dumas", tmpAddressCreateDto);
        amenityService.create(tmpAmenityCreateDto);

        List<AmenityDto> amenities = propertyService.getAllAmenitiesInRange(propertyTest.getId(), 15.0);
        assertThat(amenities.size()).isEqualTo(1);
    }
}
