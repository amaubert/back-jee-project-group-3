package esgi.project.jee.back.unit.share.validations;

import esgi.project.jee.back.share.validations.PhoneValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class PhoneValidatorTest {

    private PhoneValidator validator = new PhoneValidator();


    @ParameterizedTest
    @MethodSource("phone")
    void should_validate_or_not_the_phone(String phone, boolean result){
        assertThat(validator.isValid(phone, null)).isEqualTo(result);
    }

    private  static Stream<Arguments> phone(){
        return Stream.of(
                Arguments.of("33600000000", true),
                Arguments.of("336000000002", false),
                Arguments.of("3360000000", false),
                Arguments.of("36000000000", false),
                Arguments.of("+33600000000", false),
                Arguments.of("toto", false),
                Arguments.of("00000000000", false)
        );
    }
}
