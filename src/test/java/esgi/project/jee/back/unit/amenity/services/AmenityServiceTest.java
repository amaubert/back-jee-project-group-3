package esgi.project.jee.back.unit.amenity.services;

import esgi.project.jee.back.address.definitions.dtos.AddressCreateDto;
import esgi.project.jee.back.address.definitions.dtos.AddressDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityCreateDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityDto;
import esgi.project.jee.back.amenity.definitions.dtos.AmenityUpdateDto;
import esgi.project.jee.back.share.plateforms.gps.definitions.IAPIAddress;
import esgi.project.jee.back.address.definitions.IAddressService;
import esgi.project.jee.back.amenity.definitions.IAmenityService;
import esgi.project.jee.back.amenity.exceptions.AmenityNotFoundException;
import esgi.project.jee.back.mock.gps.AddressAPIMock;
import esgi.project.jee.back.mock.mysql.AddressRepositoryMock;
import esgi.project.jee.back.mock.mysql.AmenityRepositoryMock;
import esgi.project.jee.back.address.repositories.AddressRepository;
import esgi.project.jee.back.amenity.repositories.AmenityRepository;
import esgi.project.jee.back.address.entities.Address;
import esgi.project.jee.back.amenity.entities.Amenity;
import esgi.project.jee.back.address.services.AddressService;
import esgi.project.jee.back.amenity.services.AmenityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AmenityServiceTest {

    private IAmenityService amenityService;
    private AmenityRepository amenityRepository;

    private AddressDto addressTest;
    private AmenityDto amenityTest;

    @BeforeEach
    void setup() {
        AddressRepository addressRepository = new AddressRepositoryMock();
        IAPIAddress addressAPI = new AddressAPIMock(2.22222, 3.33333);
        IAddressService addressService = new AddressService(addressRepository, addressAPI);

        // Create an Address
        AddressCreateDto addressCreateDto = new AddressCreateDto("107", "rue Hoche", 94200, "Ivry sur Seine", "France");
        // Create an Amenity
        amenityRepository = new AmenityRepositoryMock();
        amenityService = new AmenityService(amenityRepository, addressService);
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto("Supermarché", "Auchan", addressCreateDto);

        amenityTest = amenityService.create(amenityCreateDto);
        addressTest = amenityTest.getAddress();
    }

    @Test
    void should_have_only_one_amenity() {
        try {
            assertThat(amenityRepository.count() == 1).isTrue();
        } catch (AssertionError e) {
            assertThat(e).hasMessage("The amenity was not created");
        }
    }

    @Test
    void should_have_two_amenities_at_the_same_address() {
        // Create the same new Address
        var addressCreateDto = new AddressCreateDto(addressTest);
        //create a new amenity
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto("Restaurant", "Chez Léon", addressCreateDto);
        AmenityDto newAmenityCreated = amenityService.create(amenityCreateDto);

        assertThat(newAmenityCreated).isNotNull()
                .isNotEqualTo(amenityTest);

        assertThat(newAmenityCreated.getAddress()).isNotNull()
                .isEqualTo(addressTest);

        var addressDto = newAmenityCreated.getAddress();
        assertThat(addressDto).extracting(AddressDto::getId)
                .isEqualTo(addressTest.getId());
        assertThat(addressDto).extracting(AddressDto::getPostalCode)
                .isEqualTo(addressTest.getPostalCode());
        assertThat(addressDto).extracting(AddressDto::getCity)
                .isEqualTo(addressTest.getCity());
        assertThat(addressDto).extracting(AddressDto::getPropertyNumber)
                .isEqualTo(addressTest.getPropertyNumber());
        assertThat(addressDto).extracting(AddressDto::getStreetName)
                .isEqualTo(addressTest.getStreetName());
        assertThat(addressDto).extracting(AddressDto::getCountry)
                .isEqualTo(addressTest.getCountry());
        assertThat(addressDto).extracting(AddressDto::getLatitude)
                .isEqualTo(addressTest.getLatitude());
        assertThat(addressDto).extracting(AddressDto::getLongitude)
                .isEqualTo(addressTest.getLongitude());
    }

    @Test
    void should_have_two_similar_amenities_at_different_addresses() {
        // Create a new Address
        AddressCreateDto newAddressCreateDto = new AddressCreateDto("89", "avenue de Fontainebleau", 94270, "Le Kremlin-Bicêtre", "France");

        // Create the same Amenity with the new address
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto(amenityTest);
        amenityCreateDto.setAddress(newAddressCreateDto);
        AmenityDto newAmenityCreated = amenityService.create(amenityCreateDto);

        assertThat(newAmenityCreated).isNotNull()
                .isInstanceOf(AmenityDto.class)
                .isNotEqualTo(amenityTest) // Not The same address
                .extracting(AmenityDto::getAddress)
                .isNotEqualTo(amenityTest.getAddress());
        assertThat(newAmenityCreated).extracting(AmenityDto::getId)
                .isNotNull()
                .isNotEqualTo(amenityTest.getId());
        assertThat(newAmenityCreated).extracting(AmenityDto::getName)
                .isEqualTo(amenityTest.getName());
        assertThat(newAmenityCreated).extracting(AmenityDto::getType)
                .isEqualTo(amenityTest.getType());
    }

    @Test
    void should_have_two_different_amenities_at_different_addresses() {
        // Create the same new Address
        AddressCreateDto newAddressCreateDto = new AddressCreateDto("89", "avenue de Fontainebleau", 94270, "Le Kremlin-Bicêtre", "France");

        // create an Amenity with the new address
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto("Station de Métro", "Métro ligne 7", newAddressCreateDto);
        AmenityDto newAmenityCreated = amenityService.create(amenityCreateDto);

            assertThat(newAmenityCreated).isNotNull()
                        .isInstanceOf(AmenityDto.class)
                        .isNotEqualTo(amenityTest) // Not The same address
                        .extracting(AmenityDto::getAddress)
                        .isNotEqualTo(amenityTest.getAddress());
            assertThat(newAmenityCreated).extracting(AmenityDto::getId)
                        .isNotNull()
                        .isNotEqualTo(amenityTest.getId());
            assertThat(newAmenityCreated).extracting(AmenityDto::getName)
                        .isNotEqualTo(amenityTest.getName());
            assertThat(newAmenityCreated).extracting(AmenityDto::getType)
                        .isNotEqualTo(amenityTest.getType());
    }

    @Test
    void should_get_one_amenity(){
        List<AmenityDto> list = amenityService.findAllAmenities();
        AddressCreateDto tmpAddCreateDto = new AddressCreateDto("107", "rue Hoche", 94200, "Ivry sur Seine", "France");
        Address tmpAdd = new Address(tmpAddCreateDto);
        tmpAdd.setId(1L);
        tmpAdd.setLongitude(2.22222);
        tmpAdd.setLatitude(3.33333);

        Amenity tmpAmenity = new Amenity( new AmenityCreateDto("Supermarché", "Auchan", tmpAddCreateDto));
        tmpAmenity.setId(1L);
        tmpAmenity.setAddress(tmpAdd);

        AmenityDto tmpAmenityDto = new AmenityDto(tmpAmenity);
        assertThat(list).containsOnly(tmpAmenityDto);
    }

    @Test
    void should_get_two_amenities() {
        // Create a new Amenity
        AddressCreateDto tmpAdd = new AddressCreateDto("89", "avenue de Fontainebleau", 94270, "Le Kremlin-Bicêtre", "France");
        AmenityCreateDto amenityCreateDto = new AmenityCreateDto("Station de Métro", "Métro ligne 7", tmpAdd);
        AmenityDto tmpAmenityDto = amenityService.create(amenityCreateDto);

        List<AmenityDto> list = amenityService.findAllAmenities();

        assertThat(list).containsOnly(tmpAmenityDto, amenityTest);
    }

    @Test
    void should_delete_an_amenity() throws AmenityNotFoundException {
        this.amenityService.deleteAmenityById(1L);
        assertThatThrownBy(() -> {
            this.amenityService.findAmenityById(1L);
        }).isInstanceOf(AmenityNotFoundException.class)
                .hasMessage("Amenity with ID (1) not found .");

    }

    @Test
    void should_return_false_when_delete_an_amenity_that_doesnt_exist() throws AmenityNotFoundException {
        assertThatThrownBy(() -> {
            this.amenityService.findAmenityById(2L);
        }).isInstanceOf(AmenityNotFoundException.class)
                .hasMessage("Amenity with ID (2) not found .");
    }

    @Test
    void should_update_the_entire_amenity_with_a_new_address() throws AmenityNotFoundException {
        AddressCreateDto newAddressCreateDto = new AddressCreateDto("110", "rue Michel", 94270, "Le Kremlin-Bicêtre", "France");
        AddressDto newAddressDto = new AddressDto(new Address(newAddressCreateDto));

        //Check that the new Address is not equal to the former address
        assertThat(newAddressDto).isNotEqualTo(addressTest)
                .extracting(AddressDto::getStreetName)
                .isNotEqualTo(addressTest.getStreetName());

        AmenityDto currentAmenityDto = this.amenityService.findAmenityById(1L);
        currentAmenityDto.setAddress(newAddressDto);

        AmenityUpdateDto amenityUpdateDto = new AmenityUpdateDto(currentAmenityDto);

        AmenityDto newAmenityDto = amenityService.update(1L, amenityUpdateDto);

        assertThat(newAmenityDto).isNotEqualTo(amenityTest)
                .extracting(AmenityDto::getAddress)
                .isNotEqualTo(addressTest)
                .extracting(AddressDto::getStreetName)
                .isNotEqualTo(addressTest.getStreetName());

    }

    @Test
    void should_update_the_entire_amenity() throws AmenityNotFoundException {
        //Given
        var currentAmenity = this.amenityService.findAmenityById(1L);

        assertThat(currentAmenity).isEqualTo(amenityTest); //Check equality

        currentAmenity.setType("Slow-food");
        currentAmenity.setName("Mic Danold");

        //When
        var amenityUpdateDto =  new AmenityUpdateDto(currentAmenity);
        var updatedAmenityDto = amenityService.update(currentAmenity.getId(), amenityUpdateDto);

        assertThat(updatedAmenityDto).isNotEqualTo(amenityTest)
                .extracting(AmenityDto::getType)
                .isNotEqualTo(amenityTest.getType());
        assertThat(updatedAmenityDto).extracting(AmenityDto::getName)
                .isNotEqualTo(amenityTest.getName());

        //Same Address, With haven't change this
        assertThat(updatedAmenityDto).extracting(AmenityDto::getAddress)
                .isEqualTo(amenityTest.getAddress());
    }
}
