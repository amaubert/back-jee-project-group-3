package esgi.project.jee.back.unit.token.services;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.mock.mysql.TokenRepositoryMock;
import esgi.project.jee.back.security.TokenProvider;
import esgi.project.jee.back.security.definitions.Roles;
import esgi.project.jee.back.token.definitions.services.ITokenService;
import esgi.project.jee.back.token.entities.Token;
import esgi.project.jee.back.token.services.TokenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TokenServiceTest {
    private ITokenService tokenService;
    private Account accountTest;
    private List<GrantedAuthority> authorities;

    @BeforeEach
    void setup() {
        var tokenProvider = new TokenProvider("secret",1_800_000L);
        var tokenRepository = new TokenRepositoryMock();
        tokenService = new TokenService(tokenRepository, tokenProvider);

        accountTest = new Account("login","password",true,true);

        authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(Roles.User.name()));
        authorities.add(new SimpleGrantedAuthority(Roles.Admin.name()));

    }
    @Test
    void should_create_a_token_successfully() {
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountTest.getLogin(),
                                                                            accountTest.getPassword(),
                                                                            authorities);
        var token = tokenService.create(authenticationToken, accountTest);

        assertThat(token).isNotNull()
                            .extracting(Token::getToken)
                            .isNotNull()
                            .isNotEqualTo("");
        assertThat(token.getAccount()).isNotNull()
                                        .extracting(Account::getLogin)
                                        .isEqualTo(accountTest.getLogin());
        assertThat(token.getAccount().isAdmin()).isEqualTo(accountTest.isAdmin());
        assertThat(token.getAccount().isActivated()).isEqualTo(accountTest.isActivated());
        var tokenProvider = tokenService.getTokenProvider();
        assertThat(tokenProvider.getAuthentication(token.getToken())).extracting(Authentication::getAuthorities)
                                                                        .isEqualTo(authorities);
    }

    @Test
    void should_validate_token_successfully(){
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountTest.getLogin(),
                                                                            accountTest.getPassword(),
                                                                            authorities);
        var token = tokenService.create(authenticationToken, accountTest);
        var validateToken = tokenService.validateToken(token.getToken());
        assertThat(validateToken).isTrue();
    }

    @Test
    void should_not_validate_an_invalid_token() {
        String invalidToken = "invalid Token";
        var validateToken = tokenService.validateToken(invalidToken);
        assertThat(validateToken).isFalse();
    }

    @Test
    void should_not_valid_a_token_that_is_not_created_in_base() {
        var tokenProvider = tokenService.getTokenProvider();
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountTest.getLogin(),
                accountTest.getPassword(),
                authorities);

        var jwtTokenNotCreatedInBase = tokenProvider.createToken(authenticationToken);
        var validateToken = tokenService.validateToken(jwtTokenNotCreatedInBase);
        assertThat(validateToken).isFalse();
    }

    @Test
    void should_delete_successfully_a_token() {
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountTest.getLogin(),
                accountTest.getPassword(),
                authorities);
        var token = tokenService.create(authenticationToken, accountTest);
        var validateToken = tokenService.validateToken(token.getToken());
        assertThat(validateToken).isTrue();

        tokenService.delete(token.getToken());

       validateToken = tokenService.validateToken(token.getToken());
        assertThat(validateToken).isFalse();
    }

}
