package esgi.project.jee.back.unit.user.services;


import esgi.project.jee.back.account.defintions.dtos.AccountDto;
import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.mock.mysql.AccountRepositoryMock;
import esgi.project.jee.back.mock.mysql.SponsorshipRepositoryMock;
import esgi.project.jee.back.mock.mysql.UserRepositoryMock;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorshipCreateDto;
import esgi.project.jee.back.sponsorship.services.SponsorshipService;
import esgi.project.jee.back.user.definitions.dtos.UserCreateDto;
import esgi.project.jee.back.user.definitions.dtos.UserDto;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.user.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceTest {
    private AccountRepository accountRepository;
    private UserService userService;
    private UserDto userTest;
    private AccountDto accountTest;
    private final Account defaultAccountSubscribeDto = new Account("admin", "adminadmin", true, false);
    private final UserCreateDto defaultUserAdminToCreate = new UserCreateDto("admin","admin", "33600000000",
                                                                                 "admin@myemail.com", null, null);

    @BeforeEach
    void setup() {
        var sponsorshipRepository = new SponsorshipRepositoryMock();
        accountRepository = new AccountRepositoryMock();
        var userRepository = new UserRepositoryMock();
        var smsSender = new SMSSenderServiceMock();


        var sponsorshipService = new SponsorshipService(sponsorshipRepository, smsSender);
        userService = new UserService(userRepository, sponsorshipService);

        Account account = new Account("admin", "adminadmin", true, false);
        account = accountRepository.save(account);
        accountTest = new AccountDto(account);
        defaultUserAdminToCreate.setAccount(accountTest);
        userTest = userService.create(defaultUserAdminToCreate);
    }

    @Test
    void userTest_should_be_good() {
        assertThat(userTest).isNotNull()
                            .extracting(UserDto::getFirstName)
                            .isEqualTo(defaultUserAdminToCreate.getFirstName());
        assertThat(userTest).extracting(UserDto::getLastName)
                                .isEqualTo(defaultUserAdminToCreate.getLastName());
        assertThat(userTest).extracting(UserDto::getEmail)
                                .isEqualTo(defaultUserAdminToCreate.getEmail());
        assertThat(userTest).extracting(UserDto::getPhone)
                                .isEqualTo(defaultUserAdminToCreate.getPhone());
        assertThat(userTest).extracting(UserDto::getId)
                                .isNotNull()
                                .isNotEqualTo("");
        assertThat(userTest).extracting(UserDto::getAccount)
                                .extracting(AccountDto::getId)
                                .isEqualTo(accountTest.getId());
    }


    @Test
    void should_create_a_user_successfully() {
        Account account = new Account("test", "testtest", false, false);
        account = accountRepository.save(account);
        var accountDto = new AccountDto(account);
        var  userCreateDto = new UserCreateDto("test","test", "33600000099",
                "test@myemail.com", accountDto, null);
        var userDto = userService.create(userCreateDto);

        assertThat(userDto).isNotNull()
                            .extracting(UserDto::getFirstName)
                            .isEqualTo(userCreateDto.getFirstName());
        assertThat(userDto).extracting(UserDto::getLastName)
                            .isEqualTo(userCreateDto.getLastName());
        assertThat(userDto).extracting(UserDto::getPhone)
                             .isEqualTo(userCreateDto.getPhone());
        assertThat(userDto).extracting(UserDto::getEmail)
                             .isEqualTo(userCreateDto.getEmail());
        assertThat(userDto).extracting(UserDto::getId)
                            .isNotNull()
                            .isNotEqualTo("");
        assertThat(userDto.getAccount()).extracting(AccountDto::getId)
                                            .isEqualTo(accountDto.getId());
    }

    @Test
    void should_find_user_with_the_account_id() {
        var userDto = userService.findByAccountId(accountTest.getId());

        assertThat(userDto).extracting(UserDto::getId)
                             .isEqualTo(userTest.getId());
        assertThat(userDto).extracting(UserDto::getFirstName)
                            .isEqualTo(userTest.getFirstName());
        assertThat(userDto).extracting(UserDto::getLastName)
                            .isEqualTo(userTest.getLastName());
        assertThat(userDto).extracting(UserDto::getPhone)
                            .isEqualTo(userTest.getPhone());
        assertThat(userDto).extracting(UserDto::getEmail)
                            .isEqualTo(userTest.getEmail());
        assertThat(userDto.getAccount()).isInstanceOf(AccountDto.class)
                                            .extracting(AccountDto::getId)
                                            .isEqualTo(accountTest.getId());
    }

    @Test
    void creat_sponsorship_should_be_successfull() {
        var sponsorshipCreateDto = new SponsorshipCreateDto("33600000203");
        var authenticationToken = new UsernamePasswordAuthenticationToken(defaultAccountSubscribeDto.getLogin(),
                                                                            defaultAccountSubscribeDto.getPassword());
        var  sponsorShipDto = userService.createSponsorship(sponsorshipCreateDto,authenticationToken);

        assertThat(sponsorShipDto).extracting(SponsorShipDto::getCode)
                                    .isEqualTo(SMSSenderServiceMock.lastCodeGenerated);

        assertThat(sponsorShipDto).extracting(SponsorShipDto::getSponsor)
                                        .isNotNull()
                                        .extracting(UserResponseDto::getEmail)
                                        .isEqualTo(userTest.getEmail());
        var userResponseDto = sponsorShipDto.getSponsor();
        assertThat(userResponseDto).extracting(UserResponseDto::getFirstName)
                                    .isEqualTo(userTest.getFirstName());
        assertThat(userResponseDto).extracting(UserResponseDto::getLastName)
                .isEqualTo(userTest.getLastName());
        assertThat(userResponseDto).extracting(UserResponseDto::getEmail)
                .isEqualTo(userTest.getEmail());
        assertThat(userResponseDto).extracting(UserResponseDto::getPhone)
                                    .isEqualTo(userTest.getPhone());
    }


}
