package esgi.project.jee.back.unit.share.validations;

import esgi.project.jee.back.share.validations.PasswordValidator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PasswordValidatorTest {
    private PasswordValidator validator = new PasswordValidator();

    @Test
    void should_not_validate_when_size_is_under_height_characters() {
        assertThat(validator.isValid("ccc", null)).isFalse();
    }

    @Test
    void should_not_validate_when_size_is_over_thirty_characters() {
        assertThat(validator.isValid("cccccccccccccccccccccccccccccccccccccccccccccccccc", null)).isFalse();
    }

    @Test
    void should_validate_when_size_is_over_height_characters_and_under_thirty_characters() {
        assertThat(validator.isValid("cccccccccc", null)).isTrue();
    }
}
