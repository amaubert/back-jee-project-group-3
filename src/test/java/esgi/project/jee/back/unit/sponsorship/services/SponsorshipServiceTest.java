package esgi.project.jee.back.unit.sponsorship.services;

import esgi.project.jee.back.account.entities.Account;
import esgi.project.jee.back.account.repositories.AccountRepository;
import esgi.project.jee.back.mock.mysql.AccountRepositoryMock;
import esgi.project.jee.back.mock.mysql.SponsorshipRepositoryMock;
import esgi.project.jee.back.mock.mysql.UserRepositoryMock;
import esgi.project.jee.back.mock.sms.SMSSenderServiceMock;
import esgi.project.jee.back.sponsorship.definitions.dtos.SponsorShipDto;
import esgi.project.jee.back.sponsorship.definitions.services.ISponsorshipService;
import esgi.project.jee.back.sponsorship.entities.Sponsorship;
import esgi.project.jee.back.sponsorship.exceptions.SponsorshipYourselfException;
import esgi.project.jee.back.sponsorship.repositories.SponsorshipRepository;
import esgi.project.jee.back.sponsorship.services.SponsorshipService;
import esgi.project.jee.back.user.definitions.dtos.UserResponseDto;
import esgi.project.jee.back.user.entities.User;
import esgi.project.jee.back.user.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class SponsorshipServiceTest {

    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private SponsorshipRepository sponsorshipRepository;

    private ISponsorshipService sponsorshipService;
    private User userTest;

    @BeforeEach
    void setup() {
        sponsorshipRepository = new SponsorshipRepositoryMock() ;
        userRepository = new UserRepositoryMock();
        accountRepository = new AccountRepositoryMock();
        var smsSenderService= new SMSSenderServiceMock();

        //create a User for test
        var account = new Account("admin" ,"adminadmin",true, true);
        account = accountRepository.save(account);
        var user = new User("lastname" ,"firstname","33600000000",
                                "admin@myemail.com",account);
        userTest = userRepository.save(user);

        sponsorshipService = new SponsorshipService(sponsorshipRepository, smsSenderService);
    }

    @Test
    void should_create_sponsorship_successfully(){
        var sponsorShipDto = sponsorshipService.createSponsorshipCode(userTest, "33600000001");
        assertThat(sponsorShipDto).isNotNull()
                                    .extracting(SponsorShipDto::getSponsor)
                                    .isNotNull()
                                    .extracting(UserResponseDto::getId)
                                    .isEqualTo(userTest.getId());
        assertThat(sponsorShipDto.getCode()).isEqualTo(SMSSenderServiceMock.lastCodeGenerated);
    }

    @Test
    void should_throw_an_exception_when_you_want_to_sponso_yourself(){
       assertThatThrownBy(() ->
               sponsorshipService.createSponsorshipCode(userTest, userTest.getPhone())
       ).isInstanceOf(SponsorshipYourselfException.class)
        .hasMessage("You can't sponsor yourself !");
    }

    @Test
    void should_link_sponsorship_with_the_new_user_successfully() {
        //Create new User
        var account = new Account("account" ,"accountaccount",false, true);
        account = accountRepository.save(account);
        var newUser = new User("user","user","33600990099","user@myemail.com",account);
        newUser = userRepository.save(newUser);

        var sponsorShipDto = sponsorshipService.createSponsorshipCode(userTest, newUser.getPhone());

        //Link the 2 user
        sponsorshipService.linkSponsorship(sponsorShipDto.getCode(), newUser);

        var optionalSponsorship =sponsorshipRepository.findByCode(sponsorShipDto.getCode());
        assertThat(optionalSponsorship).isPresent();
        var sponsorship = optionalSponsorship.get();

        assertThat(sponsorship).extracting(Sponsorship::getSponsor)
                                    .extracting(User::getId)
                                    .isEqualTo(userTest.getId());

        assertThat(sponsorship).extracting(Sponsorship::getSponsored)
                                .extracting(User::getId)
                                .isEqualTo(newUser.getId());
    }
}
