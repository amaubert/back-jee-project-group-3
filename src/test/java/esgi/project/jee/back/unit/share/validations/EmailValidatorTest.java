package esgi.project.jee.back.unit.share.validations;

import esgi.project.jee.back.share.validations.EmailValidator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailValidatorTest {
    private final EmailValidator emailValidator = new EmailValidator();

    @Test
    void should_validate_when_email_is_good() {
        var email = "rober.durant@gmail.com";
        assertThat(emailValidator.isValid(email, null)).isTrue();
    }

    @Test
    void should_no_validate_when_email_does_not_contain_an_arobase() {
        var email = "eepep.com";
        assertThat(emailValidator.isValid(email, null)).isFalse();
    }

    @Test
    void should_no_validate_when_email_does_not_contain_a_point() {
        var email = "fezofeofoee@gmailcom";
        assertThat(emailValidator.isValid(email, null)).isFalse();
    }
    @Test
    void should_no_validate_when_email_is_null() {
        assertThat(emailValidator.isValid(null, null)).isFalse();
    }

    @Test
    void should_no_validate_when_email_is_empty() {
        assertThat(emailValidator.isValid("", null)).isFalse();
    }
}
